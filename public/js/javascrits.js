$(function(){

	$('#dataTables').DataTable({
		"language": {
			"sLengthMenu": "_MENU_ resultados por página",
			"sZeroRecords": "Nenhum registro encontrado",
			"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			"sSearch": "Pesquisar",
			"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
			"sZeroRecords": "Nenhum registro encontrado",
			"sEmptyTable": "Nenhum registro encontrado",
			"sInfoFiltered": "(Filtrados de _MAX_ registros)",
			"sLoadingRecords": "Carregando...",
			"sProcessing": "Processando...",
			"sInfoPostFix": "",
			"sInfoThousands": ".",
			"oPaginate": {
				"sNext": "Próximo",
				"sPrevious": "Anterior",
				"sFirst": "Primeiro",
				"sLast": "Último"
			},
			"oAria": {
				"sSortAscending": ": Ordenar colunas de forma ascendente",
				"sSortDescending": ": Ordenar colunas de forma descendente"
			}
		}
	});

	$('#summernote').summernote({
		// toolbar: 	['fontsize', ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']],
		/*toolbar: [
			['style', ['style']],
			['fontsize', ['fontsize']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontname', ['fontname']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['insert', ['picture', 'hr']],
			['table', ['table']],
      ['link', ['link']],
  		],*/
		height: 283,
		lang: 'pt-BR' // default: 'en-US'
	});


	$('[data-toggle="tooltip"]').tooltip();
	$('#input').hide();

	$('#clickFoto').on('click', function(){
		// alert("oi oi oi ");
		$('#input').click();
		$('#input').change(function(){
			showImage.call(this);
		});
	});



	$(".owl-carousel").owlCarousel({
		loop:true,
		nav:true,
		margin:10,
		navText: ["<i class='material-icons'>&#xE314;</i>","<i class='material-icons'>&#xE315;</i>"],
		responsive : {
			0 : {
				items:1,
			},
			480 : {
				items:2,
			},
			1000 : {
				items:3,
			}
		}
	});



		function showImage(){
			if (this.files && this.files[0])
			var obj = new FileReader();
			obj.onload =function(data){
				image.src = data.target.result;
				image.style.display = "blocky";
			};
			obj.readAsDataURL(this.files[0]);
		};

	});
