<!DOCTYPE html>
<html>
  <head>

    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118598677-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118598677-1');
</script>


    <meta charset="utf-8">
    <meta name="description" content="desc">
    <meta name="author" content="RadonTheme">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon and Touch Icons -->
    <link rel="shortcut icon" href="images/favicon.png">
    <title>Investir Rh</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Plugin CSS -->
    <link href="{{url('css/site/bootstrap.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/site/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{url('css/site/owl.theme.default.css')}}">
    {{-- <link href="css/animate.css" rel="stylesheet"> --}}
    {{-- <link href="css/lightbox.min.css" rel="stylesheet"> --}}
    {{-- <link href="css/nice-select.css" rel="stylesheet"> --}}

    <!-- Stylesheets -->
    <link href="{{url('css/site/creative-main.css')}}" rel="stylesheet" media="screen">
    <link href="{{url('css/site/creative-responsive.css')}}" rel="stylesheet" media="screen">
    <link href="{{url('css/site/lightbox.css')}}" rel="stylesheet" media="screen">
    <link href="{{url('css/site/estilo.css')}}" rel="stylesheet" media="screen">
    <link href="{{url('https://fonts.googleapis.com/css?family=PT+Sans')}}" rel="stylesheet">
    <style type="text/css">
      body{font-family: 'PT Sans', sans-serif;}
    </style>
  </head>
  <body>

    <div class="header-top-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 text-left">
                <div class="header-top-left">
                    <div class="social-media">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 text-right">
                <div class="header-top-right">
                    <ul>
                        <li><a href="{{url('/register')}}" style="color:#fff;"> Registre-se</a></li>
                        <li><a href="{{url('/login')}}" style="color:#fff;">Login</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="header-area">
    <nav class="navbar navbar-default">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}">
                    <img src="{{url('images/logo_site.png')}}" alt="Logo" style="max-height:65px;">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="{{url('/')}}">Início</a></li>
                    <li><a href="{{url('/empresa')}}">Empresa</a></li>
                    <li><a href="{{url('/galerias')}}">Galerias</a></li>
                    <li><a href="{{url('/vagas')}}">Vagas</a></li>
                    <li><a href="{{url('/produtos')}}">Produtos</a></li>
                    <li><a href="{{url('/cursos')}}">Cursos e Treinamentos</a></li>
                    <li><a href="{{url('/blog')}}">Blog</a></li>
                    <li><a href="{{url('/site/contato')}}">Contato</a></li>
                    {{-- <li class="dropdown">
                    <a href="blog.html" class="dropdown-toggle" data-toggle="dropdown">Blog</a>
                    <ul class="dropdown-menu">
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="blog-leftsidebar.html">Blog Left Sidebar</a></li>
                    <li><a href="single-blog.html">Blog Details</a></li>
                  </ul>
                </li> --}}
                    {{-- <li><a href="{{url('/login')}}">Login</a></li> --}}

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container -->
    </nav>
</div>

    @yield('conteudo')

<div class="rodape pad">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-xs-12">
        <div class="">
          <img class="img-responsive" src="{{url('images/logo.png')}}" alt="">
        </div>
        <div class="" style="text-align:center;">
          <p>Bem vindo ao InvestiRH. Navegue por nossas vagas, Conheça nossos produtos e participe de nossos cursos<br>=)</p>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="" style="text-align:center;">
          <h3 class="titulo_site_sm mg-btm-lg">Contato</h3>
          <p>Tel:</p>
          <p>(87) 9 9990-2133</p>
          <p>(87) 9 8807-5535</p>
          <p>E-mail: contato@investirh.com</p>
        </div>
      </div>
      <div class="col-md-4 col-xs-12">
        <div class="" style="text-align:center;">
          <h3 class="titulo_site_sm mg-btm-lg">Redes Sociais</h3>
          <a href="https://www.facebook.com/investiRH.vsf/" target="_blank"> <img src="{{url('images/face.png')}}" alt=""> </a>
          <a href="https://www.instagram.com/investirhconsultoria/" target="_blank"> <img src="{{url('images/insta.png')}}" alt=""> </a>
          <p class="mg-top">Siga-nos!</p>
        </div>
      </div>
    </div>
    <div class="" style="margin-top:80px; text-align:center">
      <div class="" style="display:flex; justify-content:center; align-items:center">
      <a href="http://www.tecvalle.com.br" target="_blank"><img class="img-responsive" style="max-width:130px;" src="{{url('images/logo-tecvalle.png')}}" alt=""></a>
      </div>
      <div class="">
        <p>contato@tecvalle.com.br</p>
      </div>
    </div>
  </div>
</div>


    <!-- JavaScript -->
    {{-- <script src="js/modernizr-2.8.3.min.js"></script> --}}
    {{-- <script src="{{url('js/custom-script.js')}}"></script> --}}
    <script src="{{url('js/jquery.min.js')}}"></script>
    <script src="{{url('js/bootstrap.min.js')}}"></script>
    <script src="{{url('js/owl.carousel.min.js')}}"></script>
    <script src="{{url('js/lightbox.js')}}"></script>
    <script src="{{url('js/javascrits.js')}}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>


  </body>
</html>
