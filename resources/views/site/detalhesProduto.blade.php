@extends('site.index')
@section('conteudo')


	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">{{$produto->nome}} </h4>
		</div>
	</div>

	<div class="pad">
		<div class="container">
			<div class="row">
					<div class="col-md-3">
						<img class="thumbnail img-responsive" src="{{url('storage')}}/{{$produto->arquivo}}" alt="">
					</div>
					<div class="col-md-9">
						<h3 class="titulo_site_md mg">{{$produto->nome}}</h3>
						{{-- <span>{{$produto->created_at}}</span> --}}
						<hr>
						<p>{!!$produto->descricao!!}</p>
						<hr>
						<span> <a class="btn btn-primary" href="{{url('/produtos')}}">Voltar</a> </span>
					</div>
				</div>
		</div>
	</div>

@endsection
