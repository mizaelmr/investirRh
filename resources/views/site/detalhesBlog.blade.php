@extends('site.index')
@section('conteudo')


	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Blog</h4>
		</div>
	</div>

	<div class="pad">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					@isset($blogs->foto)
						<img class="thumbnail img-responsive" src="{{url('/storage')}}/{{$blogs->foto}}" alt="">
						@else
							<img class="thumbnail img-responsive" src="{{url('images/investir_default.jpg')}}" alt="">
					@endisset
				</div>
				<div class="col-md-9">
					<h3 class="titulo_site_md mg">{{$blogs->titulo}}</h3>
					<span>{{$blogs->created_at->format('d/m/Y - H:i')}}</span>
					<hr>
					<p>{!!$blogs->descricao!!}</p>
					<br>
					<a href="{{url('/blog')}}"><button type="button" class="btn btn-default">Voltar</button></a>
				</div>
			</div>
			<hr>
		</div>

		<div class="pad-2">
			<div class="container">
				<h3>Comentários</h3>
				<hr>
				@if (count($comentarios) > 0)
					@foreach ($comentarios as $comentario)
						<div class="row" style="padding:20px">
							<h4>{{$comentario->nome}}</h4>
							<p class="text-muted"><b>{{$comentario->created_at->format('d/m/Y')}}</b></p>
							<p class="page-text" align="justify">{{$comentario->comentario}}</p>
						</div>
						<hr>
					@endforeach
				@else
					<p>Não possui comentários!</p>
				@endif
			</div>
		</div>

		<div class="pad-2 gray" style="border-radius:8px;">
			<div class="container">
				<h3>Novo Comentário</h3>
				<div class="">
						<form class="" action="{{url('/storeComentario')}}/{{$blogs->id}}" method="POST" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="">Nome:</label>
								<input class="form-control" type="text" name="nome" required></input>
							</div>
							<div class="form-group">
								<label for="">Email:</label>
								<input class="form-control" type="email" name="email" value="" required></input>
							</div>
							<div class="form-group">
								<label for="">Comentário:</label>
								<textarea name="comentario" class="form-control" rows="8" cols="80" required></textarea>
							</div>
							{{-- <div class="g-recaptcha" data-sitekey="6LcRo1UUAAAAAJ7NMEeMVfD_km8ubWSvgd7eJbas"></div> --}}
							<div class="form-group" style="margin-top:15px; margin-bottom:15px;">
									<button type="submit" class="btn btn-success waves-effect waves-light">
										Comentar
									</button>
							</div>
						</form>
				</div>
			</div>
		</div>

	</div>

@endsection
