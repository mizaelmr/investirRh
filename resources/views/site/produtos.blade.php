@extends('site.index')
@section('conteudo')

	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Produtos e Serviços</h4>
		</div>
	</div>
	<div class="pad">
		<div class="container">
			<div class="row">
				@foreach ($produtos as $produto)
					<a href="{{url('#')}}/{{$produto->id}}">
					<div class="col-md-4 col-sm-4 col-xs-12" style="margin-bottom:15px">
						<div class="card">
							<img class="card-img-top img-fluid img-responsive" src="{{url('/storage')}}/{{$produto->arquivo}}" alt="Card image cap" width="100%">
							<div class="card-body" style="padding: 1.25rem; background: #f2f2f2; ">
								<h4 class="card-title font-20 mt-0" align="center">{{$produto->nome}}</h4>
								{{-- <center><p class="card-text" align="center">{!! mb_strimwidth($produto->descricao,0,100,"...")!!}</p></center> --}}
								<p> <a class="btn btn-success" href="{{url('/detalhesproduto')}}/{{$produto->id}}">Leia Mais...</a></p>
							</div>
						</div>
					</div>
					</a>
				@endforeach
			</div>
		</div>
	</div>

@endsection
