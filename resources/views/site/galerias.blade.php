@extends('site.index')
@section('conteudo')

	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Galerias</h4>
		</div>
	</div>

	<div class="row">
		<div class="container">
			<div class="card card-body">
				<div class="row" style="margin:80px 0px 60px 0px ;">
					@isset($galerias)
						@foreach ($galerias as $value)
							<div class="col-md-4">
								<div class="card mb-4" style="box-shadow:1px 1px 1px 1px #eaeaeaef; margin-bottom:10px; min-height:380px">
									<img class="card-img-top" alt="" style="height: 225px; width: 100%; display: block;"
									 src="@if(isset($value->fotosGaleria[0]->nome)) {{'storage/'.$value->fotosGaleria[0]->nome}} @else {{url('images/investir_default.jpg')}} @endif" data-holder-rendered="true">
									<div class="card-body" style="padding:25px">
										<h4 class="card-title">{{ $value->titulo }}</h4>
										{{---<p class="card-text">{{ $value->descricao }}</p>---}}
										<div class="d-flex justify-content-between align-items-center">
											<div class="btn-group">
												<a href="{{url('detalhegalerias/'.$value->id)}}" >
													<button type="button" class="btn btn-primary" name="button" style="margin-top:10px">Visualizar</button>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						@endforeach
					@endisset
				</div>

			</div>

		</div>
	</div>

@endsection
