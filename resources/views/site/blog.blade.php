@extends('site.index')
@section('conteudo')


	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Blog</h4>
		</div>
	</div>

	<div class="">
		<div class="container pad">
			<div class="row">
				<div class="col-md-8  col-xs-12">
					@foreach ($blogs as $blog)
						<div class="mg-btm" style="border-bottom:solid 1px #f2f2f2; padding:20px 0px;">
							<div class="row">
								<div class="col-md-3">
									@isset($blog->foto)
										<img class="thumbnail img-responsive" src="{{url('/storage')}}/{{$blog->foto}}" alt="">
										@else
											<img class="thumbnail img-responsive" src="{{url('images/investir_default.jpg')}}" alt="">
									@endisset
								</div>
								<div class="col-md-9">
									<h3 class="titulo_site_md mg">{{$blog->titulo}}</h3>
									<span>{{$blog->created_at->format('d/m/Y')}}</span>
									<p>{!! mb_strimwidth($blog->descricao,0,250,"...")!!}</p>
									<p> <a href="{{url('/detalhesblog')}}/{{$blog->id}}">Leia Mais...</a></p>
								</div>
							</div>
						</div>
					@endforeach
				</div>

				<div class="col-md-4 col-xs-12 divisor">

					<div class="">
						<h4 class="titulo_site_md mg-btm">Produtos</h4>
					</div>

					<div class="">
						<div class="owl-carousel" id="produtos">
							@if(count($produtos) > 0)
								@foreach ($produtos as $produto)
									<div style="border: solid 1px #d8d8d8;">
										<div class="">
											<img class="img-responsive" src="{{url('/storage')}}/{{$produto->arquivo}}" alt="">
										</div>
										<div class="" style="padding:10px; background:#fff;">
											<p class="titulo_site_sm">{{$produto->nome}}</p>
											{{-- <span>{!!$produto->descricao!!}</span> --}}
											<p><a href="{{url('/detalhesproduto')}}/{{$produto->id}}">Mais Detalhes...</a></p>

										</div>
									</div>
								@endforeach
							@else
								<div style="border: solid 1px #d8d8d8;">
									<div class="">
										<img class="img-responsive" src="{{url('images/investir_default.jpg')}}" alt="">
									</div>
									<div class="" style="padding:10px; background:#fff;">
										<p>Cursos e Treinamentos</p>
										<span>Visite nossa página de produtos</span>
									</div>
								</div>
							@endif
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<script src="{{url('js/jquery.min.js')}}"></script>
	<script src="{{url('js/owl.carousel.min.js')}}"></script>
	<script type="text/javascript">

	$("#produtos").owlCarousel({
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true,
		loop:true,
		navText: ["<i class='material-icons nav-prod'>&#xE314;</i>","<i class='material-icons nav-prod'>&#xE315;</i>"],
		responsive : {
			0 : {
				items:1,
				nav:true,

			},
			480 : {
				items:1,
				nav:true,

			},
			1000 : {
				items:1,
				nav:true,

			}}
		});

		</script>


	@endsection
