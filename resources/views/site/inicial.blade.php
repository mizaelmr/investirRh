@extends('site.index')
@section('conteudo')

	@if (count($slides) > 0)
		<div class="slider">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					@if (isset($slides[0]))
						<div class="item active">
							<img src="storage/{{$slides[0]->foto}}" alt="...">
						</div>
					@endif
					@for ($i=1; $i < count($slides); $i++)
						<div class="item">
							<img src="storage/{{$slides[$i]->foto}}" alt="...">
						</div>
					@endfor
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Voltar</span>
				</a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Próximo</span>
				</a>
			</div>
		</div>
	@endif

	<div class=" pad">
		<div class="container flex">
			<div class="col-md-8 col-xs-12">
				<h2 class="titulo_site_lg mg-btm">{{$dados[0]->titulo}}</h2>
				<p>{!!$dados[0]->descricao!!}</p>
			</div>
			<div class="col-md-4 col-xs-12">
				<img class="img-responsive" src="{{url('/storage')}}/{{$dados[0]->foto}}" alt="{{$dados[0]->foto}}" title="{{$dados[0]->foto}}">
			</div>

		</div>
	</div>

	<div class="pad gray">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-xs-12">

					<div class="">
						<h3 class="titulo_site_md mg-btm">Vagas</h3>
					</div>

					@if(count($vagas) > 0)
					<div class="">
							@foreach ($vagas as $vaga)
								<div class="card-vaga row" style="background:#fff;">
									<div class="col-md-5 col-xs-12">
										<h4><b>Vaga: </b></h4>
										<p>{{$vaga->titulo}}</p>
									</div>
									<div class="col-md-5 col-xs-12">
										<h4><b>Formação: </b></h4>
										<p>{{$vaga->formacao}}</p>
									</div>
									<div class="col-md-2 col-xs-12">
										<a href="{{url('/detalhesvagas')}}/{{$vaga->id}}">
											<button type="button" class="btn btn-primary">
												Detalhes
											</button>
										</a>
									</div>
								</div>
							@endforeach
							<div class=""style="margin-top:30px; display:flex">
								<a href="/vagas"><button type="button" class="btn btn-primary" style="margin-right:20px;">Todas</button></a> {{ $vagas->links() }}
							</div>
						</div>
					@else
						<div class="card-vaga row" style="background:#fff;">
							<p> =( Não possuem vagas disponiveis no momento. ! </p>
						</div>
					@endif
			</div>

			<div class="col-md-4 col-xs-12 divisor">
				<div class="">
					<h4 class="titulo_site_md mg-btm">Produtos</h4>
				</div>

				<div class="">
					<div class="owl-carousel" id="produtos">
						@if(count($produtos) > 0)
							@foreach ($produtos as $produto)
								<div style="border: solid 1px #d8d8d8;">
									<div class="">
										<img class="img-responsive" src="storage/{{$produto->arquivo}}" alt="">
									</div>
									<div class="" style="padding:10px; background:#fff;">
										<p class="titulo_site_sm">{{$produto->nome}}</p>
										{{-- <span>{!!$produto->descricao!!}</span> --}}
										<p><a href="{{url('/detalhesproduto')}}/{{$produto->id}}">Mais Detalhes...</a></p>

									</div>
								</div>
							@endforeach
						@else
							<div style="border: solid 1px #d8d8d8;">
								<div class="">
									<img class="img-responsive" src="{{url('images/investir_default.jpg')}}" alt="">
								</div>
								<div class="" style="padding:10px; background:#fff;">
									<p>Cursos e Treinamentos</p>
									<span>Visite nossa página de produtos</span>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="pad">
	<div class="container">
		<div class="">
			<h3 class="titulo_site_md mg-btm">Clientes</h3>
		</div>
		<div class="owl-carousel" id="clientes">
			@foreach ($clientes as $cliente)
				<div >
					<div class="col-md-12">
						<img class="thumbnail" src="storage/{{$cliente->foto}}" alt="{{$cliente->titulo}}">
					</div>
				</div>
			@endforeach

		</div>
	</div>
</div>

<div class=" pad gray">
	<div class="container">
		<div class="">
			<h3 class="titulo_site_md mg-btm">Parceiros</h3>
		</div>
		<div class="row">
			@foreach ($parceiros as $parceiro)
				<div class="col-xs-12 col-md-3">
					<a href="#" class="thumbnail">
						<img src="storage/{{$parceiro->foto}}" alt="{{$parceiro->titulo}}">
					</a>
				</div>
			@endforeach
		</div>
	</div>
</div>

<script src="{{url('js/jquery.min.js')}}"></script>
<script src="{{url('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">
$("#clientes").owlCarousel({
	autoplay:true,
	autoplayTimeout:2000,
	autoplayHoverPause:true,
	loop:true,
	navText: ["<i class='material-icons'>&#xE314;</i>","<i class='material-icons'>&#xE315;</i>"],
	responsive : {
		0 : {
			items:1,
		},
		480 : {
			items:2,
			nav:true,
		},
		1000 : {
			items:4,
			nav:true,
		}}
	});


	$("#produtos").owlCarousel({
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true,
		loop:true,
		navText: ["<i class='material-icons nav-prod'>&#xE314;</i>","<i class='material-icons nav-prod'>&#xE315;</i>"],
		responsive : {
			0 : {
				items:1,
				nav:true,

			},
			480 : {
				items:1,
				nav:true,

			},
			1000 : {
				items:1,
				nav:true,

			}}
		});
	</script>

@endsection
