@extends('site.index')
@section('conteudo')


	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Cursos</h4>
		</div>
	</div>
	<div class="pad">
		<div class="container">
			<div class="">
				@foreach ($ferramentas as $ferramenta)
					<div class="col-md-4 col-lg-4 col-xl-3">
						<div class="card m-b-30">
							<a href=""><img class="card-img-top img-fluid img-responsive" src="{{url('/storage')}}/{{$ferramenta->arquivo}}" alt="Card image cap" width="100%"></a>
							<div class="card-body" style="padding: 1.25rem; background: #f2f2f2;">
								<h4 class="card-title font-20 mt-0" align="center">{{$ferramenta->nome}}</h4>
								{{-- <center><p class="card-text" align="center">{!!$ferramenta->descricao!!}</p></center> --}}
								<p> <a class="btn btn-success" href="{{url('/detalhescurso')}}/{{$ferramenta->id}}">Leia Mais...</a></p>

							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

@endsection
