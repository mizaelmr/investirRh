@extends('site.index')
@section('conteudo')

	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Contato</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
						</button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="pad ">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="">
						<h2>Fale com a gente!</h2>
						{{---<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br> tempor incididunt ut labore et dolore magna aliqua.</p>---}}
					</div>
					<div class="" style="margin-top:60px;">
						<div style="margin:30px 0px; font-size:18px;"><i class="fa fa-map-marker" aria-hidden="true"></i> Juazeiro - BA</div>
						<div style="margin:30px 0px; font-size:18px;"><i class="fa fa-phone-square" aria-hidden="true"></i> Tel: (87) 9 8807-5535</div>
						<div style="margin:30px 0px; font-size:18px;"><i class="fa fa-envelope" aria-hidden="true"></i> Email: contato@investirh.com</div>
					</div>
				</div>

				<div class="col-md-6">
					<div class="card" style="padding: 15px; border-radius: 4px; box-shadow: 1px 1px 1px 1px #c7c7c7; background: #fbfbfb;">
						<div class="">
							<h2>Formulário de Contato</h2>
						</div>
						<form class="" action="{{url('/site/formcontato')}}" method="post">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="">Nome:</label>
								<input name="nome" class="form-control" value="" type="text">
							</div>
							<div class="form-group">
								<label for="">Assunto:</label>
								<input name="assunto" class="form-control" value="" type="text">
							</div>
							<div class="form-group">
								<label for="">E-mail:</label>
								<input name="email" class="form-control" value="" type="email">
							</div>
							{{-- <div class="form-group">
							<label for="">Telefone:</label>
							<input name="tel" class="form-control" value="" type="text">
						</div> --}}
						<div class="form-group">
							<label for="">Mensagem:</label>
							<textarea name="descricao" class="form-control" rows="8" cols="80"></textarea>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-success">Enviar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
