@extends('site.index')
@section('conteudo')

	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Sobre nós</h4>
		</div>
	</div>


<div class="">
	<div class="row pad" style="border:solid 1px #f2f2f2;">
		<div class="container">
			<h3 class="titulo_site_lg mg-btm">{{$empresa[0]->titulo}}</h3>
			<p style="text-align:justify;">{!!$empresa[0]->descricao!!}</p>
		</div>
	</div>

	<div class="row pad">
		<div class="container">
			<div class="col-md-4 mg-sm">
				<h3 class="titulo_site_md mg-btm">{{$empresa[1]->titulo}}</h3>
				<p >{!!$empresa[1]->descricao!!}</p>
			</div>
			<div class="col-md-4 mg-sm">
				<h3 class="titulo_site_md mg-btm">{{$empresa[2]->titulo}}</h3>
				<p style="text-align:justify;">{!!$empresa[2]->descricao!!}</p>
			</div>
			<div class="col-md-4 mg-sm">
				<h3 class="titulo_site_md mg-btm">{{$empresa[3]->titulo}}</h3>
				<p style="text-align:justify;">{!!$empresa[3]->descricao!!}</p>
			</div>
		</div>
	</div>
</div>

@endsection
