@extends('site.index')
@section('conteudo')


	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Detalhes Vagas</h4>
		</div>
	</div>

	<div class="pad">
		<div class="container">
			<div class="row" style="padding: 25px ;border-bottom:solid 1px #efefef; border-top:solid 1px #efefef;">
				<div class="col-md-3 mg-top">
					<h4><b>Vaga:</b></h4>{{$vaga->titulo}}
				</div>
				<div class="col-md-3 mg-top">
					<h4><b>Formação:</b> </h4>{{$vaga->formacao}}
				</div>
				<div class="col-md-3 mg-top">
					<h4><b>Local:</b> </h4>{{$vaga->local}}
				</div>
				<div class="col-md-3 mg-top">
					<a href="{{url('/admin/candidatarVaga')}}/{{$vaga->id}}"> <button type="button" class="btn btn-success">
						Candidatar-se
					</button></a>
				</div>
			</div>
			<div class="row mg-top-lg">
				<div class="col-md-4">
					<h3>Informações:</h3>
					<ul>
						<li class="gray"><b>N°. vagas:</b> {{$vaga->num_vagas}}</li>
						<li><b>Atribuições:</b> {{$vaga->Atribuicoes}}</li>
						<li class="gray"><b>Experiência:</b> {{$vaga->experiencia}}</li>
						<li><b>Beneficios:</b> {{$vaga->beneficios}}</li>
						<li class="gray"><b>Sexo:</b> {{$vaga->sexo}}</li>
					</ul>
				</div>
				<div class="col-md-8">
					<h3>Descrição:</h3>
					{!!$vaga->descricao!!}
				</div>
			</div>
		</div>
	</div>

@endsection
