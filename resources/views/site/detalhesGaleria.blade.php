@extends('site.index')
@section('conteudo')

	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Galerias</h4>
		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
		<div class="card card-body" style="margin:80px 0px 60px 0px ;">
			<div class="container">
				<div class="" style="text-align:center">
					<h1>{{$galeria->titulo}}</h1>
					<span style="margin:10px 0px">{{$galeria->descricao}}</span>
				</div>
				<hr>

				<div class="">
					<div class="row">
						@foreach ($galeria->fotosGaleria as $foto)
							<div class="col-md-3">
								<div class="img">
									<a href="{{url('storage/'.$foto->nome)}}" data-lightbox="roadtrip">
									<img src="{{url('storage/'.$foto->nome)}}" alt="..." class="img-responsive" >
								</a>
								</div>
								<div class="btnDelete" style="margin-top:6px; background: #f4f4f4;">
									@can ('admin_access')
										<a href="{{'deleteFoto/'.$foto->id}}">
											<button type="button" class="btn btn-danger" name="button">Excluir</button>
										</a>
									@endcan
								</div>
							</div>
						@endforeach
					</div>
				</div>

				<div class="" style="margin-top:60px">
					<a href="{{url()->previous()}}">
					<button type="button" class="btn btn-primary" name="button">Voltar</button>
					</a>
				</div>

			</div>
		</div>
	</div>

</div>

	@endsection
