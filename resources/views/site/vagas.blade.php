@extends('site.index')
@section('conteudo')

	<div class="pad-2 gray">
		<div class="container">
			<h4 class="titulo_site_md">Vagas</h4>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-xs-12 col-sm-6">
				<div class="box_search" style="text-align:center;">
					<p>Exibindo <span class="number">{{count($vagas)}}</span> de <span class="number">{{$CountVagas}}</span> Totais disponíveis</p>
				</div>
			</div>
			<div class="col-md-6 col-xs-12 col-sm-6 col-md-right">
				<div class="box_search">
					<form class="form-inline" action="{{url('/buscar')}}" method="get">
						{{ csrf_field() }}
						<div class="form-group">
							<label class="sr-only" for="exampleInputAmount">Buscar</label>
							<div class="input-group">
								<div class="input-group-addon"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></div>
								<input type="text" name="buscar" value="" class="form-control" id="exampleInputAmount" placeholder="Buscar por nome da vaga">
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Buscar</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="gray pad-2">
		<div class="container " style="margin-bottom:40px;">
			@if(count($vagas) > 0)
				@foreach ($vagas as $vaga)
					<div class="card-vaga row" style="background:#fff;">
						<div class="col-md-3 col-sm-3 col-xs-12 mg">
							<p><b>Vaga: </b>{{$vaga->titulo}}</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 mg">
							<p><b>Formação: </b>{{$vaga->formacao}}</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 mg">
							<p><b>Local: </b>{{$vaga->local or "Não informado!"}}</p>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 mg">
							<a href="{{url('/detalhesvagas')}}/{{$vaga->id}}">
								<button type="button" class="btn btn-primary">
									Detalhes
								</button>
							</a>
						</div>
					</div>
				@endforeach
				<br>
				{{ $vagas->links() }}
			@else
				<div class="row pad gray" style="text-align:center; border-radius:20px;">
					<h1>Ops</h1>
					<p>Infelizmente não encontramos o que você procura!</p>
				</div>
			@endif
		</div>
	</div>

@endsection
