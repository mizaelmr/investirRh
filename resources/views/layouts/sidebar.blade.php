<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>Investir RH</title>

		<!-- Styles -->
		<title>Investir RH</title>
		<meta content="Investir RH" name="Sua melhor opção em empregos">
		<meta content="Tecvalle" name="Mizael Duarte">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- App Icons -->
		<link rel="shortcut icon" href="images/favicon.ico">


		<!-- App css -->
		<link href="{{ url('css/app.css') }}" rel="stylesheet">
		<link href="{{url('css/admin/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('css/admin/style.css')}}" rel="stylesheet" type="text/css">
		<link href="{{url('css/admin/estilos.css')}}" rel="stylesheet" type="text/css">

		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

		<link rel="stylesheet" href="{{url('css/admin/datatables/datatables.css')}}"/>
		<link rel="stylesheet" href="{{url('css/admin/datatables/dataTables.bootstrap4.css')}}"/>
		{{-- <link rel="stylesheet" href="{{url('css/admin/datatables/dataTables.min.css')}}"/>
		<link rel="stylesheet" href="{{url('css/admin/datatables/dataTables.bootstrap4.min.css')}}"/> --}}

		<link rel="stylesheet" href="{{url('css/admin/summernote-bs4.css')}}">

		<link rel="stylesheet" href="{{url('css/site/owl.carousel.css')}}">
		<link rel="stylesheet" href="{{url('css/site/owl.theme.default.css')}}">
		{{-- <link href="css/admin/icons.css" rel="stylesheet" type="text/css"> --}}
</head>
<body style="overflow: visible;">

					<!-- Loader -->
					<div id="preloader" style="display: none;"><div id="status" style="display: none;"><div class="spinner"></div></div></div>

					<!-- Navigation Bar-->
					<header id="topnav">
							<div class="topbar-main">
									<div class="container-fluid">

											<!-- Logo container-->
											<div class="logo">
													<!-- Text Logo -->
													{{-- <a href="index.html" class="logo">
													Investir RH
													</a> --}}
													<!-- Image Logo -->
													<a href="{{url('/admin/home')}}" class="logo">
															<img src="{{url('images/logo.png')}}" alt="" height="50" class="logo-small">
															<img src="{{url('images/logo.png')}}" alt="" height="60" class="logo-large">
													</a>
											</div>

											  <div class="menu-extras topbar-custom">

													<ul class="list-inline float-right mb-0">

															<li class="list-inline-item dropdown notification-list">
																	<a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
																	 aria-haspopup="false" aria-expanded="false">
																		<img src="{{url('images/user.png')}}" alt="user" class="rounded-circle">
																	</a>
																	<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
																		@can('admin_access')
																			<a class="dropdown-item" href="{{url('/admin/contas')}}">Contas</a>
																			<a class="dropdown-item" href="{{url('/admin/categorias')}}">Categorias</a>
																			<div class="dropdown-divider"></div>
																		@endcan
																			<a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
																			   <i class="dripicons-exit text-muted"></i> Sair
																			</a>
																			<form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
																			    {{ csrf_field() }}
																			</form>
																	</div>
															</li>

															<li class="menu-item list-inline-item">
																	<!-- Mobile menu toggle-->
																	<a class="navbar-toggle nav-link">
																			<div class="lines">
																					<span></span>
																					<span></span>
																					<span></span>
																			</div>
																	</a>
																	<!-- End mobile menu toggle-->
															</li>

													</ul>
												</div>

											<!-- end menu-extras -->
											<div class="clearfix"></div>
									</div> <!-- end container -->
							</div>
							<!-- end topbar-main -->
							<!-- MENU Start -->
							<div class="navbar-custom active">
									<div class="container-fluid">
											<div id="navigation" class="active">
													<!-- Navigation Menu-->
													<ul class="navigation-menu">

															<li class="has-submenu active">
																	<a href="{{ url('/admin/home') }}"><i class="material-icons">&#xE871;</i>Dashboard</a>
															</li>
															@can('admin_access')
																<li class="has-submenu">
																		<a href="#"><i class="material-icons">supervisor_account</i>Site</a>
																		<ul class="submenu">
																			<li><a href="{{ url('/admin/pginicial') }}">Texto Inicial</a></li>
																			<li><a href="{{ url('/admin/empresa') }}">Empresa</a></li>
																			<li><a href="{{ url('/admin/slides') }}">Slides</a></li>
																			<li><a href="{{ url('/admin/galerias') }}">Galerias</a></li>
																		</ul>
																</li>
															@endcan
																	<li class="has-submenu">
																			<a href="{{ url('/admin/vagas') }}"><i class="material-icons">build</i>Vagas</a>
																	</li>
															@can('admin_access')
																	<li class="has-submenu">
																			<a href="{{ url('/admin/blog') }}"><i class="material-icons">chat</i>Blog</a>
																	</li>

																	<li class="has-submenu">
																			<a href="{{ url('/admin/clientes') }}"><i class="material-icons">supervisor_account</i>Clientes e Parceiros</a>
																			<ul class="submenu">
																				<li><a href="{{ url('/admin/clientes') }}">Clientes</a></li>
																				<li><a href="{{ url('/admin/parceiros') }}">Parceiros</a></li>
																			</ul>
																	</li>
																	<li class="has-submenu">
																			<a href="{{ url('/admin/produtos') }}"><i class="material-icons">find_in_page</i>Produto e Cursos</a>
																		<ul class="submenu">
																			<li><a href="{{ url('/admin/produtos') }}">Produtos</a></li>
																			<li><a href="{{ url('/admin/cursos') }}">Cursos</a></li>
																		</ul>
																	</li>
																	<li class="has-submenu">
																			<a href="{{ url('/admin/candidatos') }}"><i class="material-icons">find_in_page</i>Seleção</a>
																		<ul class="submenu">
																			<li><a href="{{ url('/admin/candidatos') }}">Candidatos</a></li>
																			<li><a href="{{ url('/admin/candidaturas') }}">Inscrição</a></li>
																		</ul>
																	</li>
																	<li class="has-submenu">
																			<a href="{{ url('/admin/contato') }}"><i class="material-icons">perm_phone_msg</i>Contato</a>
																	</li>
															@endcan
															@can('client_access')
																<li class="has-submenu">
																		<a href="#"><i class="material-icons">assignment_ind</i>Meus dados</a>
																	<ul class="submenu">
																		<li><a href="{{ url('/admin/dados') }}">Dados Cadastrais</a></li>
																		<li><a href="{{ url('/admin/curriculo') }}">Currículo</a></li>
																	</ul>
																</li>
															@endcan

													</ul>
											</div> <!-- fim da navegação -->
									</div> <!-- fim do container -->
							</div> <!-- fim da navegação geral -->
					</header>




					<div class="wrapper ">
						<div class="container-fluid">
							@yield('content')
						</div>
					</div>

					<footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        © 2018 Sistemas <a href="http://www.tecvalle.com.br" target="_blank"> Tecvalle </a>- Soluções em sistemas - Investir RH.
                    </div>
                </div>
            </div>
        </footer>

					<!-- jQuery  -->
				<script src="{{url('js/jquery.min.js')}}"></script>
				{{-- <script src="js/popper.min.js')}}"></script> --}}
				<script src="{{url('js/bootstrap.min.js')}}"></script>
				<script src="{{url('js/modernizr.min.js')}}"></script>
				<script src="{{url('js/datatables/datatables.min.js')}}"></script>
				<script src="{{url('js/datatables/datatables.bootstrap4.min.js')}}"></script>
				<script src="{{url('js/summernote-bs4.js')}}"></script>
				<script src="{{url('js/summernote-pt-BR.js')}}"></script>
				<script src="{{url('js/javascrits.js')}}"></script>
				<script src="{{url('js/owl.carousel.min.js')}}"></script>
				{{-- <script src="js/waves.js"></script> --}}
				{{-- <script src="js/jquery.slimscroll.js"></script>
				<script src="js/jquery.nicescroll.js"></script>
				<script src="js/jquery.scrollTo.min.js"></script> --}}

				<!--Morris Chart-->
				{{-- <script src="plugins/morris/morris.min.js"></script> --}}
				{{-- <script src="plugins/raphael/raphael-min.js"></script> --}}
				{{-- <script src="pages/dashborad.js"></script> --}}

				<!-- App js -->
				<script src="{{url('js/app_2.js')}}"></script>

				@yield('javascript')



</body>
</html>
