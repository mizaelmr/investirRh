@extends('layouts.sidebar')
@section('content')


	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/home') }}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Histórico</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>


	@if (isset($candidatura[0]))
	<div class="row">
		<div class="col-12">
			<div class="card m-b-30">
				<div class="" style="Padding:40px;">
					<div class="">
						<div class="m-b-12">
							<section id="cd-timeline" class="cd-container">
								@foreach ($candidatura as $value)
									<div class="cd-timeline-block">
	                  <div class="cd-timeline-img cd-success">
	                      <i class="mdi mdi-adjust" ></i>
	                  </div> <!-- cd-timeline-img -->

	                  <div class="cd-timeline-content">
	                      <h3>Vaga: {{$value->vaga->titulo}}</h3>
												<p class="mb-0 text-muted font-14">{!!$value->vaga->descricao!!}</p>
												@isset($value->created_at)
													<span class="cd-date">{{$value->created_at->format('d/m/Y')}}</span>
												@endisset
												<a href="{{url('/admin/showVagas')}}/{{$value->vaga->id}}"><button type="button" class="btn btn-primary">Detalhes</button></a>
	                  </div> <!-- cd-timeline-content -->
	                </div>
								@endforeach
							</section>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	@else
		<div class="row">
			<div class="col-12">
				<div class="card m-b-30">
					<div class="" style="Padding:40px;">
						<div class="">
							<div class="m-b-12">
								<section id="cd-timeline" class="cd-container">
									<center><p class="mb-0 text-muted font-14">Você não se cadastrou em nenhuma vaga!</p></center>
								</section>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	@endif







	@endsection
