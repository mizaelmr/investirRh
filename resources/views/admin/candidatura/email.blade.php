<div class="container" style="width: 40%;margin-top:40px; margin-left: 30%; align-items: center;">
	<div class="col-md-6 col-md-offset-3 col-xs-12" style=" ">
		<div class="" style="display:flex; justify-content:center; margin-bottom:50px;">
			<center><img class="img-responsive" src="{{url('images/logo_site.png')}}" alt="" style="max-height:90px;"></center>
		</div>
		<div class="box2" style="background:#f2f2f2; padding:40px; border-radius: 6px; text-align: center;">
			<h2>Confirmação de Candidatura</h2><br>
			<p>Obrigado <span style="text-transform:uppercase;"><b>{{$dados->name or ""}}</b></span> por se candidatar em nossas vagas!</p>
			<p>
				<a href="http://www.investirh.com" target="_blank">
					<button type="button" style="padding: 15px; background: #1a4483; color: #fff; border: none; border-radius: 3px;">Ir para o site</button>
				</a>
			</p>
		</div>
	</div>
</div>
