@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/home') }}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Histórico</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>


	<div class="row">
		<div class="col-12">
			<div class="card m-b-30">
				<div class="">
					<div class="">
						<div class="m-b-12">
							<section id="cd-timeline" class="cd-container">
								@foreach ($candidatura as $value)
									<div class="cd-timeline-block">
	                  <div class="cd-timeline-img cd-success">
	                      <i class="mdi mdi-adjust" ></i>
	                  </div> <!-- cd-timeline-img -->

	                  <div class="cd-timeline-content">
	                      <h3>Vaga: {{$value->vaga->titulo}}</h3>
                      <li><b>Candidato:</b> {{$value->usuario->name}}</li>
                        <hr>
												{{---<p class="mb-0 text-muted font-14">{!!$value->vaga->descricao!!}</p>---}}
												@isset($value->created_at)
													<span class="cd-date">{{$value->created_at->format('d/m/Y')}}</span>
												@endisset
												<a href="{{url('/admin/candidatoCurriculo')}}/{{$value->users_id}}"><button type="button" class="btn btn-primary">Currículo</button></a>
	                  </div> <!-- cd-timeline-content -->
	                </div>
								@endforeach
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan






	@endsection
