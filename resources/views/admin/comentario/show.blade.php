@extends('layouts.sidebar')
@section('content')

	@can ('admin_access')
		<div class="row">
			<div class="col-sm-12">
				<div class="page-title-box">
					<div class="btn-group pull-right">
						<ol class="breadcrumb hide-phone p-0 m-0">
							<a href="{{url('/admin/home')}}">
								<li class="btn btn-info breadcrumb-item active">Voltar</li>
							</a>
						</ol>
					</div>
					<h4 class="page-title">Comentário</h4>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="card card-body">
					<div class="container">

						<div class="" style="text-align:center; margin-bottom:20px;">
							<h1 class="card-title">{{$comentarios->nome}}</h2>
								<p class="text-muted m-b-30 font-14">
									Email: {{$comentarios->email}} | Data: {{$comentarios->created_at->format('d/m/Y')}}</p>
								</div>

								<div class="">
									<div class="mg-btm-lg col-md-4 col-md-offset-4" style="text-align:center;">
										<h3 style="text-transform:uppercase;">{{$blog->titulo}}</h3>
										<hr>
										<h3><b>Comentário:</b></h3>
										{{$comentarios->comentario}}
									</div>

									<div class="col-md-4 col-md-offset-4">
										<a href="{{url('/admin/aprovarComentario')}}/{{$comentarios->id}}"><i data-toggle="tooltip" data-placement="top" title="Ativar/Desativar" class="material-icons btn btn-success">&#xE876;</i></a>
										<a href="{{url('/admin/deleteComentario')}}/{{$comentarios->id}}"><i data-toggle="tooltip" data-placement="top" title="Excluir" class="material-icons btn btn-danger">&#xE92B;</i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endcan

			@can ('client_access')
				<div class="wrapper-page">

					<div class="card">
						<div class="card-block">

							<div class="ex-page-content text-center">
								<h1 class="">404!</h1>
								<h3 class="">Página não encontrada!</h3><br>

								<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
							</div>

						</div>
					</div>
				</div>
			@endcan




		@endsection
