@extends('layouts.sidebar')
@section('content')

	@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/vagas')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Formulário de cadastro vaga</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
		<div class="card card-body">
			<div class="">
				<h4 class="mt-0 header-title">Cadastro de vagas</h4>
				<p class="text-muted m-b-30 font-14">Preencha os campos para inserir uma nova vaga</p>
			</div>
			<div class="dropdown-divider"></div>
			<form class="" action="{{url('/admin/storeVagas')}}" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						  <label for="">*Titulo da vaga</label>
						  <input type="text" class="form-control"  value="{{old('titulo')}}" name="titulo" placeholder="" >
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">*Numero de vaga</label>
								  <input type="number" class="form-control"  value="{{old('num_vagas')}}" name="num_vagas" placeholder="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">*Formação</label>
								  <input type="text" class="form-control"  value="{{old('formacao')}}" name="formacao" >
								</div>
							</div>
					</div>
						<div class="form-group">
						  <label for="">*Experiências</label>
						  <input type="text" class="form-control"  value="{{old('experiencia')}}" name="experiencia" >
						</div>
						<div class="form-group">
						  <label for="">Beneficios</label>
						  <input type="text" class="form-control"  value="{{old('beneficios')}}" name="beneficios">
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Sexo</label>
								  <select class="form-control"  value="{{old('sexo')}}" name="sexo">
										<option value="Indiferente">Indiferente</option>
										<option value="Masculino">Masculino</option>
										<option value="Feminino">Feminino</option>
								  </select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Local</label>
								  <input type="text" class="form-control"  value="{{old('local')}}" name="local" placeholder="">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Estilo de contrato</label>
									<select class="form-control"  value="{{old('categorias_id')}}" name="categorias_id">
										<option value="">Selecione</option>
										@foreach ($categorias as $cat)
											<option value="{{$cat->id}}">{{$cat->nome}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Empresa</label>
									<input type="text" class="form-control"  value="{{old('empresa')}}" name="empresa" value="">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">

						<div class="form-group">
						  <label for="">*Descrição</label>
						  <textarea id="summernote"  value="{{old('descricao')}}" name="descricao" class="form-control" rows="8" cols="80" ></textarea>
						</div>
					</div>
				</div>
				<div class="dropdown-divider"></div>
				<div class="form-group">
					<button type="submit" class="btn btn-info btn-lg">Cadastrar</button>
				</div>
			</form>
		</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

            <div class="card">
                <div class="card-block">

                    <div class="ex-page-content text-center">
                        <h1 class="">404!</h1>
                        <h3 class="">Página não encontrada!</h3><br>

                        <a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
                    </div>

                </div>
            </div>
        </div>
@endcan
@endsection
