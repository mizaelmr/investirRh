@extends('layouts.sidebar')
@section('content')

<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
					{{ session('msg') }}
				</div>
			@endif
			@if (session('msg1'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
					{{ session('msg1') }}
				</div>
			@endif
			@if (session('msg2'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
						</button>
					{{ session('msg2') }}
				</div>
			@endif
			@if (session('msg3'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">×</span>
						</button>
					{{ session('msg3') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						
						<a href="{{url('/admin/home')}}">

							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Vagas</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="row">
					<div class="col-md-6">
						<h4>Lista de vagas cadastradas</h4>
						<span class="text-muted m-b-30 font-14">Numero de Vagas: {{count($dados)}}</span>
					</div>
					@can('admin_access')
					<div class="col-md-6 just-end">
						<a href="{{url('/admin/createVagas')}}">
							<button type="button" class="btn btn-success btn-lg">NOVO</button>
						</a>
					</div>
					@endcan
				</div>
				<div class="dropdown-divider"></div>
				<div style="margin-top:40px;">

					<table class="table table-striped"  data-ordem="" >
						<thead>
							<tr>
								<th style="display:none">Codigo</th>
								<th>Titulo</th>
								<th>Categoria</th>
								<th>Formação</th>
								@can('admin_access')
								<th>Empresa</th>
								<th>Status da vaga</th>
								@endcan
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>

							@foreach ($dados as $value)
								<tr>
									<td style="display:none">{{$value->id or " Não Possui dados."}}</td>
									<td>{{$value->titulo or " Não Possui dados."}}</td>
									<td>{{$value->categoria->nome or " Não Possui dados."}}</td>
									<td>{{$value->formacao or " Não Possui dados."}}</td>
									@can('admin_access')
									<td>{{$value->empresa or " Não Possui dados."}}</td>
										@if ($value->ativo == '0')
											<td style="color:red;">Desativa</td>
										@else
											<td style="color:green;">Ativa</td>
										@endif
									@endcan
									<td>
										<a href="{{url('/admin/showVagas')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Detalhes" class="material-icons btn btn-info">&#xE873;</i></a>
										@can('admin_access')
										<a href="{{url('/admin/editVagas')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Editar" class="material-icons btn btn-warning">&#xE8D7;</i></a>
										<a href="{{url('/admin/deleteVagas')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Excluir" class="material-icons btn btn-danger">&#xE92B;</i></a>
										<a href="{{url('/admin/ativoVagas')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Ativar/Desativar" class="material-icons btn btn-success">&#xE876;</i></a>
									@endcan
									@php
										//dd(count($user->candidatura) );
										//$contador = $contador + 1;
										$contador = -1;
									@endphp
											@can('client_access')
												@if (count($user->candidatura) <> 0)
													@foreach ($user->candidatura  as $candidatou)
														@php
															$contador = $contador + 1;
														@endphp
														@if ($contador <= (count($user->candidatura) - 1))
															@if ($user->candidatura[$contador]->vagas_id == $value->id)
																<a><i data-toggle="tooltip" data-placement="top" title="Candidatado" class="material-icons btn btn-danger">&#xE862;</i></a>
																@php
																	break;
																@endphp
															@else
																@if ($contador == (count($user->candidatura) - 1))
																	<a href="{{url('/admin/candidatarVaga')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Candidatar" class="material-icons btn btn-success">&#xE862;</i></a>
																@endif
															@endif
														@else
															<a href="{{url('/admin/candidatarVaga')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Candidatar" class="material-icons btn btn-success">&#xE862;</i></a>
														@endif
													@endforeach
												@else
													<a href="{{url('/admin/candidatarVaga')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Candidatar" class="material-icons btn btn-success">&#xE862;</i></a>
												@endif
											@endcan
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('javascript')
	<script type="text/javascript">
		$('.table').DataTable({
			order:[[0,"desc"]],
			"language": {
				"sLengthMenu": "_MENU_ resultados por página",
				"sZeroRecords": "Nenhum registro encontrado",
				"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
				"sSearch": "Pesquisar",
				"sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
				"sZeroRecords": "Nenhum registro encontrado",
				"sEmptyTable": "Nenhum registro encontrado",
				"sInfoFiltered": "(Filtrados de _MAX_ registros)",
				"sLoadingRecords": "Carregando...",
				"sProcessing": "Processando...",
				"sInfoPostFix": "",
				"sInfoThousands": ".",
				"oPaginate": {
					"sNext": "Próximo",
					"sPrevious": "Anterior",
					"sFirst": "Primeiro",
					"sLast": "Último"
				},
				"oAria": {
					"sSortAscending": ": Ordenar colunas de forma ascendente",
					"sSortDescending": ": Ordenar colunas de forma descendente"
				}
			}

		});


	</script>
@endsection
