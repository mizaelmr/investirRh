@extends('layouts.sidebar')
@section('content')

	@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/vagas')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Editar vaga</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
		<div class="card card-body">
			<div class="">
				<h4 class="mt-0 header-title">Cadastro de vagas</h4>
				<p class="text-muted m-b-30 font-14">Preencha os campos para inserir uma nova vaga</p>
			</div>
			<div class="dropdown-divider"></div>
			<form class="" action="{{url('/admin/updateVagas')}}/{{$vaga->id}}" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						  <label for="">*Titulo da vaga</label>
						  <input type="text" class="form-control" name="titulo" value="{{$vaga->titulo}}" >
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Numero de vaga</label>
								  <input type="number" class="form-control" name="num_vagas" value="{{$vaga->num_vagas}}">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">*Formação</label>
								  <input type="text" class="form-control" name="formacao" value="{{$vaga->formacao}}" >
								</div>
							</div>
					</div>
						<div class="form-group">
						  <label for="">*Experiências</label>
						  <input type="text" class="form-control" name="experiencia" value="{{$vaga->titulo}}" >
						</div>
						<div class="form-group">
						  <label for="">Beneficios</label>
						  <input type="text" class="form-control" name="beneficios" value="{{$vaga->beneficios}}">
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Sexo</label>
								  <select class="form-control" name="sexo">
										<option value="Indiferente">Indiferente</option>
										<option value="Masculino">Masculino</option>
										<option value="Feminino">Feminino</option>
								  </select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Local</label>
								  <input type="text" class="form-control" name="local" value="{{$vaga->local}}" >
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Estilo de contrato</label>
									<select class="form-control" name="categorias_id">
										<option value="">Selecione</option>
										@foreach ($categorias as $cat)
											<option value="{{$cat->id}}">{{$cat->nome}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">Empresa</label>
									<input type="text" class="form-control" name="empresa" value="{{$vaga->empresa}}">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">

						<div class="form-group">
						  <label for="">*Descrição</label>
						  <textarea id="summernote" name="descricao" class="form-control" rows="8" cols="80" >{{$vaga->descricao}}</textarea>
						</div>
					</div>
				</div>
				<div class="dropdown-divider"></div>
				<div class="form-group">
					<button type="submit" class="btn btn-info btn-lg">Atualizar</button>
				</div>
			</form>
		</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

						<div class="card">
								<div class="card-block">

										<div class="ex-page-content text-center">
												<h1 class="">404!</h1>
												<h3 class="">Página não encontrada!</h3><br>

												<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
										</div>

								</div>
						</div>
				</div>
@endcan
@endsection
