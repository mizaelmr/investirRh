@extends('layouts.sidebar')
@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/vagas')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Vagas</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body" style="padding:40px">
				<div class="row">
				<div class="col-md-8">
					<h2 class="card-title">Vaga: <span style="text-transform:uppercase;">{{$vaga->titulo or ""}}</span></h2>
				</div>
				<div class="col-md-4">
					<div class="m-b-30 ">
						<h4 class="card-title font-20 mt-0" style="text-align: center; font-size:32px">{{count($candidatura)}}</h4>
						<p class="card-text" style="text-align: center;"> Número de candidatos</p>
					</div>
				</div>
			</div>
				<hr>
				<div class="row">
					<div class="col">
						@can ('admin_access')
							<p class="card-text"><b>Empresa:</b> {{$vaga->empresa or ""}}</p>
						@endcan
						<p class="card-text"><b>Numero de vagas:</b> {{$vaga->num_vagas or ""}}</p>
						<p class="card-text"><b>Formação:</b> {{$vaga->formacao or ""}}</p>
					</div>
					<div class="col">
						<p class="card-text"><b>Experiências:</b> {{$vaga->experiencia or ""}}</p>
						<p class="card-text"><b>Beneficios:</b> {{$vaga->beneficios or ""}}</p>
						<p class="card-text"><b>Sexo:</b> {{$vaga->sexo or ""}}</p>
					</div>
					<div class="col">
						<p class="card-text"><b>Local:</b> {{$vaga->local or ""}}</p>
						<p class="card-text"><b>Contrato:</b> {{$vaga->titulo or ""}}</p>
						@can ('admin_access')
							<p class="card-text"><b>Vaga Ativa:</b>@if($vaga->ativo === 0) Não  @else Sim @endif</p>
							@endcan
						</div>
					</div>
					<hr>
					<div class="">
						<h4 class="card-text">Descrição:</h4>
						<p class="card-text">{!!$vaga->descricao or ""!!}</p>
					</div>
				</div>
			</div>
		</div>

		@can ('admin_access')
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="card card-body" style="padding:40px; margin-bottom:40px">
						<div class="row">
							<div class="col-md-6">
								<h4>Lista de candidatos para essa vaga</h4>
								<span class="text-muted m-b-30 font-14">Número de candidatos: {{count($candidatura)}}</span>
							</div>
						</div>
						<div class="dropdown-divider"></div>
						<div style="margin-top:40px;">

							<table class="table table-striped" id="dataTables" >
								<thead>
									<tr>
										<th>Nome</th>
										<th>E-mail</th>
										{{-- <th>Cel</th> --}}
										<th>Ações</th>
									</tr>
								</thead>
								<tbody>

									@foreach ($candidatura as $value)
										<tr>
											<td><span style="text-transform:uppercase; font-size:13px;">{{$value->usuario->name or " Não Possui dados."}}<span></td>
											<td>{{$value->usuario->email or " Não Possui dados."}}</td>
											{{-- <td>{{$value->dados}}</td> --}}
											<td>
												<a href="{{url('/admin/candidatoCurriculo')}}/{{$value->usuario->id}}"><i data-toggle="tooltip" data-placement="top" title="Detalhes" class="material-icons btn btn-info">&#xE873;</i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		@endcan
	@endsection
