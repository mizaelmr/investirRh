@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/blog')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
						<a href="{{url('/admin/editBlog')}}/{{$blog->id}}" >
							<li class="btn btn-warning breadcrumb-item active" style="margin-left:10px; color:#fff">Editar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Blog</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="container">
					<div class="top" style="text-align:center; margin-bottom:20px;">
						<h1 class="card-title">{{$blog->titulo,0,10}}</h2>
						<p class="text-muted m-b-30 font-14">Autor: {{$blog->usuario->name}}
						| Data: {{$blog->created_at->format('d/m/Y')}}</p>
					</div>
					<div class="">
						{!!$blog->descricao!!}
					</div>
					</div>
				</div>
			</div>
		</div>

	@endcan

	@can ('client_access')
		<div class="wrapper-page">

			<div class="card">
				<div class="card-block">

					<div class="ex-page-content text-center">
						<h1 class="">404!</h1>
						<h3 class="">Página não encontrada!</h3><br>

						<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
					</div>

				</div>
			</div>
		</div>
	@endcan




	@endsection
