@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/home')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Blog</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="row">
					<div class="col-md-6">
						<h4>Lista informações divulgadas</h4>
						<span class="text-muted m-b-30 font-14">Informações no Blog: {{count($dados)}}</span>
					</div>
					<div class="col-md-6 just-end">
						<a href="{{url('/admin/createBlog')}}">
							<button type="button" class="btn btn-success btn-lg">NOVO</button>
						</a>
					</div>
				</div>
				<div class="dropdown-divider"></div>
				<div style="margin-top:40px;">

					<table class="table table-striped" id="dataTables" >
						<thead>
							<tr>
								<th>Código</th>
								<th>Título</th>
								{{-- <th>Descrição</th> --}}
								<th>Autor</th>
								<th>Data</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($dados as $value)

								<tr>
									<td>{{$value->id or " Não Possui dados."}}</td>
									<td>{{ mb_strimwidth($value->titulo,0,70,"...") }}</td>
									{{-- <td>{!!$value->descricao or " Não Possui dados."!!}</td> --}}
									<td>{{$value->usuario->name or " Não Possui dados."}}</td>
									<td>{{$value->created_at->format('d/m/Y')}}</td>
									<td>
										<a href="{{url('/admin/showBlog')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Detalhes" class="material-icons btn btn-info">&#xE873;</i></a>
										<a href="{{url('/admin/editBlog')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Editar" class="material-icons btn btn-warning">&#xE8D7;</i></a>
										<a href="{{url('/admin/deleteBlog')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Excluir" class="material-icons btn btn-danger">&#xE92B;</i></a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan

@endsection
