@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/blog')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Formulário de cadastro do Blog</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="">
					<h2 class="mt-0 header-title">Cadastrando tópico</h2>
				</div>
				<div class="row">
				<div class="col-md-9">
				<form class="" action="{{url('/admin/storeBlog')}}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="">Título:</label>
						<input class="form-control" name="titulo" value="" ></input>
					</div>
					<div class="form-group">
						<label for="">Descrição</label>
						<textarea id="summernote" name="descricao" class="form-control" rows="16" cols="80" ></textarea>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success">
							Salvar
						</button>
					</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-default" style="margin-top:26px;">
				  <div class="panel-heading">
				    <h3 class="panel-title">
							Foto de capa
						</h3>
				  </div>
				  <div class="panel-body">
						<img class="img-responsive" src="" id="image">
				  </div>
					<div class="panel-footer">
						<button type="button" class="btn btn-success" id="clickFoto" name="button">Inserir foto de capa</button>
						<input id="input" name="foto" type="file"></input>
					</div>
				</div>
			</form>
				</div>
			</div>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan

@endsection
