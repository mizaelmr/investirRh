@extends('layouts.sidebar')
@section('content')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/curriculo')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Edição de currículo</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>


		<div class="col-md-12">

				<div class="">
					<h2 class="mt-0 header-title">Editando</h2>
				</div>
				<div class="row">
				<div class="col-md-12">
				<form class="" action="{{url('/admin/updateCurriculo')}}/{{$info->id}}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="">Currículo</label>
						<textarea id="summernote" name="descricao" class="form-control" rows="16" cols="80">{{$info->descricao}}</textarea>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-success">
							Atualizar
						</button>
					</div>
			</div>
				</form>

		</div>
	</div>

@endsection
