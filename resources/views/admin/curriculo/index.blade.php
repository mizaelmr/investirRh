@extends('layouts.sidebar')
@section('content')
	<!-- CSS para impressão -->
	{{-- <link rel="stylesheet" type="text/css" href="{{url('css/admin/print.css')}}" media="print" /> --}}
	<style type="text/css">

	@media print{
		*{
			background: transparent !important;
			color:#000 !important;
			box-shadow: none !important;
			text-shadow: none !important;
			margin:3px 0px 0px 0px !important;
			padding:0px !important;
		}
		#no-print{display: none;}
	}
	.print{padding: 40px;  margin-bottom: 40px;}
</style>


<div class="row">
	<div class="col-sm-12">
		<div class="page-title-box">
			<div class="btn-group pull-right">
				<ol class="breadcrumb hide-phone p-0 m-0">
					<a href="{{ URL::previous() }}">
						<li class="btn btn-info breadcrumb-item active">Voltar</li>
					</a>
				</ol>
			</div>
			<h4 class="page-title">Visualizando Currículo</h4>
		</div>
	</div>
</div>

<div class="col-md-12">
	<div class="container" id="no-print">
		<button type="button" onClick="window.print()" class="btn btn-default">
			Imprimir
		</button>
		<hr>
	</div>
	<div class="card container print">
		<div class="row">
			<div class="col" style="text-align:center;">
				@if (isset($dados->foto))
					<img class=" rounded-circle" src="{{url('/storage')}}/{{$dados->foto}}" height="128" width="128">
				@else
					<img class=" rounded-circle" src="{{url('images/investir_default.jpg')}}" height="128" width="128">
				@endif
				<h4 style="text-transform: uppercase;">{{$dados->nomeComp or ""}}</h4>
				<p ><b>{{$dados->atuacao or ""}}</b><br>
				<p ><b>Nascimento:</b> {{$dados->nascimento or ""}}<br>
					<b>CNH:</b> {{$dados->cnh or ""}}</p>
				</div>
				<div class="col">
					<h2>INFORMAÇÕES</h2>
					<hr>
					@isset($dados->rua)
					<p><b>Endereço: </b>{{$dados->rua.','}} @isset($dados->numero){{$dados->numero.','}}@endisset  {{$dados->bairro.','}} {{$dados->cidade.','}} {{$dados->estado}}.</p>
					@endisset
						<p><b>E-mail: </b>{{$user->email or ""}}</p>
						<p><b>Telefone: </b>{{$dados->tel or ""}}</p>
						<p><b>Celular: </b>{{$dados->cel or ""}}</p>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col">
						<h2>EDUCAÇÃO</h2>
						<hr>
						@foreach ($ensino as $valor)
							<h4 style="font-size: 15px; text-transform: uppercase;"><i class="material-icons" style="font-size:10px; margin-right:5px">label</i>{{$valor->instituicao or ""}} - {{$valor->curso or ""}}</b></h4>
							<p>{{$valor->descricao or ""}}</p>
						@endforeach
					</div>
				</div>
				<div class="row">
					<div class="col">
						<h2>EXPERIÊNCIA</h2>
						<hr>
						@foreach ($experiencia as $value)
							<h4 style="font-weight:700; font-size: 15px; text-transform: uppercase;">{{$value->titulo or ""}}</h4>
							<p><b style="display: flex; align-items: center;"><i class="material-icons" style="font-size:10px; margin-right:5px">label</i>{{$value->empresa or ""}}</b></p>
							<p style="text-align:justify; border-bottom: solid 1px #e0e0e0; padding-bottom: 10px;">{{$value->descricao or ""}}</p>
						@endforeach
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
		imprimir(){
			window.print();
		}
	</script>

@endsection
