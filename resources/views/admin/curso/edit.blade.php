@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/cursos')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Formulário de cadastro de cursos</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<h2 class="mt-0 header-title">Criando novo curso</h2>
				<form class="" action="{{url('/admin/updateCursos')}}/{{$dados->id}}" method="POST" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="">Imagem:</label>
						<input type="file" name="arquivo" value="{{$dados->arquivo}}">
					</div>
					<div class="form-group">
						<label for="">Título:</label>
						<input class="form-control" name="nome" value="{{$dados->nome}}" ></input>
					</div>
					<div class="form-group">
						<label for="">Descrição:</label>
						<textarea id="summernote"  name="descricao" class="form-control" rows="5">{!!$dados->descricao!!}</textarea>
					</div>
					<div class="form-group">
						<div>
							<button type="submit" class="btn btn-primary waves-effect waves-light">
								Salvar
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan

@endsection
