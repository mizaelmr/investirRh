@extends('layouts.sidebar')
@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/dados') }}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Editar dados</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
		<div class="card card-body">
			<div class="">
				<h4 class="mt-0 header-title">Dados Cadastrais</h4>
				<p class="text-muted m-b-30 font-14">Preencha os campos para editar seu cadastro</p>
			</div>
			<div class="dropdown-divider"></div>
			<form class="" action="{{url('/admin/updateDados')}}/{{$user->id }}" method="post" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-12">
						<div class="row">
						<div class="col-md-6">

							<div class="form-group">
								<label for="">*Nome completo</label>
								<input type="text" class="form-control" name="nomeComp" value="{{$dados->nomeComp or old('nomeComp') }}" >
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="">*Data Nascimento</label>
									<input type="date" class="form-control" name="nascimento" value="@isset($dados->nascimento){{$dados->nascimento}}@endisset" required>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="">*CNH</label>
								<select class="form-control" name="cnh" value="{{$dados->cnh or old('cnh')}}">
									<option value="">Selecione...</option>
									<option value="Não Possuo">Não Possuo</option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="C">C</option>
									<option value="D">D</option>
									<option value="E">E</option>
									<option value="AB">AB</option>
									<option value="AC">AC</option>
									<option value="AD">AD</option>
									<option value="AE">AE</option>
								</select>
							</div>
						</div>
						</div>{{--primeira linha de inputs--}}
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								  <label for="">*Email</label>
								  <input type="text" class="form-control" name="email" value="{{$user->email or old('email') }}" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">*Rua</label>
									<input type="text" class="form-control" name="rua" value="{{$dados->rua or old("rua")}}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								  <label for="">Número</label>
								  <input type="number" class="form-control" name="numero" value="{{$dados->numero or old('cep')}}" >
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								  <label for="">*CEP</label>
								  <input type="text" class="form-control" name="cep" value="{{$dados->cep or old('cep') }}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								  <label for="">*Bairro</label>
								  <input type="text" class="form-control" name="bairro" value="{{$dados->bairro or old('bairro')}}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
								  <label for="">*Cidade</label>
								  <input type="text" class="form-control" name="cidade"  value="{{$dados->cidade or old('cidade')}}">
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="">*Estado</label>
								  <input type="text" class="form-control" name="estado" value="{{$dados->estado or old('estado')}}" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">Imagem:</label>
									<input class="form-control" name="foto" type="file" ></input>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">*Principal área de atuação</label>
									<input type="text" class="form-control" name="atuacao" value="{{$dados->atuacao or old('atuacao')}}" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">Telefone</label>
									<input type="text" class="form-control" name="tel" value="{{$dados->tel or old('tel')}}" >
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="">*Celular</label>
								  <input type="text" class="form-control" name="cel" value="{{$dados->cel or old('cel')}}" >
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="dropdown-divider"></div>
				<div class="form-group">
					<button type="submit" class="btn btn-info btn-lg">Atualizar</button>
				</div>
			</form>
		</div>
		</div>
	</div>
@endsection
