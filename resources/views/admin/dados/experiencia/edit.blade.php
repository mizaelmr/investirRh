@extends('layouts.sidebar')
@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/dados') }}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Editar Experiência</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
		<div class="card card-body">
			<div class="">
				<h4 class="mt-0 header-title">Experiência</h4>
				<p class="text-muted m-b-30 font-14">Preencha os campos editar sua experiência</p>
			</div>
			<div class="dropdown-divider"></div>
			<form class="" action="{{url('/admin/updateExperiencia')}}/{{$dados->id}}" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
						  <label for="">*Titulo</label>
						  <input type="text" class="form-control" name="titulo" value="{{$dados->titulo or old('titulo') }}" >
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">*Empresa</label>
								  <input type="text" class="form-control" name="empresa" value="{{$dados->empresa or old('cep') }}">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
								  <label for="">*Período</label>
								  <input type="text" class="form-control" name="periodo" id="periodo" value="{{$dados->periodo or old('periodo') }}" >
								</div>
							</div>
					</div>
						<div class="form-group">
						  <label for="">*Cidade</label>
						  <input type="text" class="form-control" name="cidade" value="{{$dados->cidade or ""}}" >
						</div>
						<div class="form-group">
						  <label for="">*Estado</label>
						  <input type="text" class="form-control" name="estado"  id="estado" value="{{$dados->estado or ""}}">
						</div>
						<div class="form-group">
						  <label for="">*Descrição</label>
						  <textarea id="" name="descricao" class="form-control" rows="6" cols="" >{{$dados->descricao or ""}}</textarea>
						</div>

					</div>

				</div>
				<div class="dropdown-divider"></div>
				<div class="form-group">
					<button type="submit" class="btn btn-info btn-lg">Atualizar</button>
				</div>
			</form>
		</div>
		</div>
	</div>
@endsection
