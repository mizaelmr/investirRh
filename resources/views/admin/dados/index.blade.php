@extends('layouts.sidebar')
@section('content')

	<style type="text/css">
		.tit_capi{text-transform: capitalize;}
		.tit_upper{text-transform: uppercase;}
		.texto_exp{margin-left: 15px;}
	</style>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/home') }}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Dados Cadastrais</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="card col-md-12">
		<div class="">
			<div class="col-md-4" style="border-right: solid 1px #f2f2f2; padding:50px 20px; text-align:center" >
				@if (isset($dados->foto))
					<img class=" rounded-circle" src="{{url('/storage')}}/{{$dados->foto}}" height="128" width="128">
				@else
					<img class=" rounded-circle" src="{{url('images/investir_default.jpg')}}" height="128" width="128">
				@endif
				<h4 class="tit_upper">{{$dados->nomeComp or "" }}</h4>
				<p><b class="tit_upper">{{$dados->atuacao or ""}}</b></p>

				<p><b>Nascimento:</b> @isset($dados) {{date( 'd/m/Y', strtotime($dados->nascimento))}}  @endisset</p>
				<p style="text-transform:uppercase;"><b>CNH:</b> {{$dados->cnh or "" }}</p>
				<p class="text-muted">Cadastrado em: @if (isset($user->created_at)){{$user->created_at->format('d/m/Y')}}@endif</p>
					<hr>
					<a href="{{url('/admin/editDados')}}"><button type="button" class="btn btn-warning">Editar</button></a>
				</div>
				<div class="col-md-8" style="border-left: solid 1px #f2f2f2; padding:50px;">
					<div class="row">
						<div class="col">
							<h4>Endereço:</h4>
							<li><b>Endereço:</b> {{$dados->rua or "" }}</li>
							<li><b>Bairro:</b> {{$dados->bairro or "" }}</li>
							<li><b>Cidade:</b> {{$dados->cidade or "" }}</li>
							<li><b>Estado:</b> {{$dados->estado or "" }} </li>
							<li><b>CEP:</b> {{$dados->cep or "" }}</li>
						</div>
						<div class="col">
							<h4>Contato:</h4>
							<li><b>Email:</b> {{$user->email or "" }}</li>
							<li><b>Celular:</b> {{$dados->tel or ""}}</li>
							<li><b>Telefone:</b> {{$dados->cel or ""}}</li>
						</div>
					</div>
					<hr>
					{{-- <div class="row">
					<div class="col">
					<h4><a href="{{$dados->facebook or ""}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Click para abrir">Facebook</a> </h4>
				</div>
				<div class="col">
				<h4><a href="{{$dados->instagram or ""}}" target="_blank" data-toggle="tooltip" data-placement="top" title="Click para abrir">Instagram</a> </h4>
			</div>
		</div> --}}
	</div>
</div>
</div>

{{-- Instituições --}}
<div class="card col-md-12" style="margin-top:20px; padding:10px;">
	<div class="row">
		<div class="col-md-11">
			<h4><b>Formação</b></h4>
		</div>
		<div class="col-md-1 ClassCenter" style="padding:5px;">
			<a href="{{url('/admin/createEnsino')}}" >
				<button type="button" class="btn btn-info btn-lg">NOVO</button>
			</a>
		</div>
	</div>
	<hr>

	@foreach($ensino as $valor)
		<div class="col-md-12" style="box-shadow:0px 1px 1px 1px #d3d3d3; margin-top:20px; padding:20px 20px;">
			<div class="row">
				<div class="col">
					<h2 class="tit_capi">{{$valor->instituicao or ''}}</h2>
					<p class="texto_exp tit_capi">
						<b class="tit_upper">{{$valor->curso or ''}}</b> - {{date( 'd/m/Y', strtotime($valor->dataInic))}} / {{date( 'd/m/Y', strtotime($valor->dataFim))}}<br>
						<p class="texto_exp">{{$valor->descricao or ''}}</p>
					</div>
				</div>
				<hr>
				<a href="{{url('/admin/editEnsino')}}/{{$valor->id or ''}}"><button type="button" class="btn btn-warning">Editar</button></a>
				<a href="{{url('/admin/deleteEnsino')}}/{{$valor->id or ''}}"><button type="button" class="btn btn-danger">Excluir</button></a>
			</div>
		@endforeach
	</div>

	{{-- Experiencia --}}
	<div class="card col-md-12" style="margin:20px 0px; padding:30px;">
		<div class="row">
			<div class="col-md-11">
				<h4><b>Experiência</b></h4>
			</div>
			<div class="col-md-1 ClassCenter" style="padding:5px;">
				<a href="{{url('/admin/createExperiencia')}}" >
					<button type="button" class="btn btn-info btn-lg">NOVO</button>
				</a>
			</div>
		</div>
		<hr>

		@foreach($experiencia as $value)
			<div class="col-md-12" style="margin-top:20px; padding:20px 20px; border-bottom: 1px dashed #d3d3d3;">
				<div class="row">
					<div class="col">
						<h2 class="tit_capi">{{$value->titulo}}</h2>
						<p class="texto_exp tit_capi">
							<b class="tit_upper">{{$value->empresa}}</b> - {{$value->periodo}}<br>
							{{$value->cidade}}, {{$value->estado}}</p>
							<p class="texto_exp" >{{$value->descricao}}</p>
						</div>
					</div>
					<hr>
					<a href="{{url('/admin/editExperiencia')}}/{{$value->id}}"><button type="button" class="btn btn-warning">Editar</button></a>
					<a href="{{url('/admin/deleteExperiencia')}}/{{$value->id}}"><button type="button" class="btn btn-danger">Excluir</button></a>
				</div>
			@endforeach
		</div>



	</div>




@endsection
