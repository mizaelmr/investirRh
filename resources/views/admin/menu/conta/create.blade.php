@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/contas')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Administrador</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<h2 class="mt-0 header-title">Criando novo Administrador</h2>
				<form class="" action="{{url('/admin/newUser')}}" method="post">
					{{ csrf_field() }}
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
							  <label for="">Nome:</label>
							  <input type="text" class="form-control" name="name" value="" >
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
									  <label for="">Email:</label>
									  <input type="text" class="form-control" name="email" value="">
									</div>
								</div>
							</div>
							<div class="form-group">
							  <label for="">Senha:</label>
							  <input type="password" class="form-control" name="password" value="" >
							</div>
							<div class="form-group">
							  <label for="">Confirmar Senha:</label>
							  <input type="password" class="form-control" name="confirm_password" value="" >
							</div>
							<div class="form-group">
							  <input type="hidden" class="form-control" name="tipo_users"  id="estado" value="1">
							</div>
						</div>
					</div>
					<div class="dropdown-divider"></div>
					<div class="form-group">
						<button type="submit" class="btn btn-info btn-lg">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan


	@endsection
