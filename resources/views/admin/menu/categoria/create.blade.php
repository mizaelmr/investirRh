@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/categorias')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Formulário de cadastro de categoria</h4>
			</div>
		</div>
	</div>

	{{-- ERRO --}}
	<div class="row">
		<div class="col-md-12">
			@if ($errors->any())
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
						@foreach ($errors->all() as $erro)
							{{ $erro }}
						@endforeach
				</div>
			@endif
		</div>
	</div>

	<div class="row">
	<div class="col-md-12">
		<div class="card card-body">
			<div class="">
				<h4 class="mt-0 header-title">Cadastro de categoria</h4>
				<p class="text-muted m-b-30 font-14">Preencha os campos para inserir uma nova categoria</p>
			</div>
			<div class="dropdown-divider"></div>
			<form class="" action="{{url('/admin/storeCateg')}}" method="post">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
						  <label for="">Nome</label>
						  <input type="text" class="form-control" name="nome" placeholder="" >
						</div>
					</div>
				</div>
				<div class="dropdown-divider"></div>
				<div class="form-group">
					<button type="submit" class="btn btn-info btn-lg">Cadastrar</button>
				</div>
			</form>
		</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan
@endsection
