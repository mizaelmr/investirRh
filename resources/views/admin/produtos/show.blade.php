@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/produtos')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">{{$produtos->nome}}</h4>
			</div>
		</div>
	</div>

	<div class="col-md-4 col-lg-4 col-xl-3">
		<div class="card m-b-30" >
			<img class="card-img-top img-fluid img-responsive" src="/storage/{{$produtos->arquivo}}" alt="Card image cap" width="100%">
			<div class="card-body" style="padding: 1.25rem; background: #fff;">
				<h4 class="card-title font-20 mt-0" align="center">{{$produtos->nome}}</h4>
				<p class="card-text" align="center">{!!$produtos->descricao!!}</p>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan




	@endsection
