@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/contato')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Responder E-mail</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">

				<div class="container">
				<div class="" style="background:#f4f4f5; border-radius:5px; padding:20px; border:solid 1px #c3c3c3;">
					<div class="">
						<div class="">
							<h2>E-mail</h2>
						</div>
						<div class="row">
							<div class="col">
								<span class="text-muted"><b>De:</b> {{$info->email}}</span>
							</div>
							<div class="col">
								<span class="text-muted"><b>Recebido:</b> {{$info->created_at}}</span>
							</div>
						</div>
					</div>
					<hr>
					<div class="">
						<h1>{{$info->assunto}}</h1>
						<span class="text-muted"><b>Autor:</b> {{$info->nome}}</span>
						<hr>
						<p class="card-text">{{$info->descricao}}</p>
					</div>
				</div>
				</div>
				<div class="container">
					<br>
					<hr>

				<form class="" action="{{url('/admin/submitResposta')}}/{{$info->id}}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="">Resposta:</label>
						<textarea name="resposta" class="form-control" rows="5" required></textarea>
					</div>
					<div class="form-group">
							<button type="submit" class="btn btn-success waves-effect waves-light">
								Enviar
							</button>
					</div>
				</form>
			</div>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan

@endsection
