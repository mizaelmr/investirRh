@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/home')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Contato</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="row">
					<div class="col-md-6">
						<h4>Lista solicitações de contato</h4>
						<span class="text-muted m-b-30 font-14">Número de Contatos: {{count($dados)}}</span>
					</div>

				</div>
				<div class="dropdown-divider"></div>
				<div style="margin-top:40px;">

					<table class="table table-striped" id="dataTables" >
						<thead>
							<tr>
								<th>Assunto</th>
								<th>Nome</th>
								<th>Email</th>
								{{-- <th>Descrição</th> --}}
								<th>Data</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($dados as $value)
								<tr>
									<td>{{$value->assunto or " Não Possui dados."}}</td>
									<td>{{$value->nome or " Não Possui dados."}}</td>
									<td>{{$value->email or " Não Possui dados."}}</td>
									{{-- <td>{{$value->descricao or " Não Possui dados."}}</td> --}}
									<td>{{$value->created_at or " Não Possui dados."}}</td>
									<td><a href="{{url('/admin/respostaContato')}}/{{$value->id}}" title="Visualizar/Responder"><i data-toggle="tooltip" data-placement="top" title="Responder Email" class="material-icons btn btn-info">&#xE0BE;</i></a></td>
								</tr>
							@endforeach
							</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan

@endsection
