@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/home')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Clientes</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="row">
					<div class="col-md-6">
						<h3>Listagem de Clientes:</h3>
						<span class="text-muted m-b-30 font-14">Numero de Clientes: {{count($dados)}}</span>
					</div>
					<div class="col-md-6 just-end">
						<a href="{{url('/admin/createClientes')}}">
							<button type="button" class="btn btn-success btn-lg">NOVO</button>
						</a>
					</div>
				</div>
				<div class="dropdown-divider"></div>
				<div class="">
					@foreach ($dados as $value)
						<div class="col-md-2" style="  margin-bottom: 10px; padding:10px;">
							<div class="" style="border: solid 1px #c3c3c3; padding-bottom: 10px;">
							<a href="#" class="" style="margin-bottom:0px;">
								<img class="rounded mr-2 img-responsive" src="{{url('/storage')}}/{{$value->foto}}" alt="{{$value->nome}}" title="{{$value->nome}}">
							</a>
							<h4 style="text-align: center; margin:10px 0px;">{{$value->nome}}</h4>
							<hr>
							<a href="{{url('/admin/deleteClientes')}}/{{$value->id}}" style="margin-left:10px;"><i class="material-icons btn btn-danger">&#xE92B;</i></a>
						</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan



@endsection
