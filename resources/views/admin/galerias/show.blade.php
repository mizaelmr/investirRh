@extends('layouts.sidebar')

@section('content')

	<div class="row">
		<div class="col-md-12">
			@if (session('msg-success'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg') }}
				</div>
			@endif

			@if (session('msg-error'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<a href="{{url()->previous()}}">
						<li class="btn btn-info breadcrumb-item active">Voltar</li>
					</a>
				</div>
				<h4 class="page-title">Fotos</h4>
			</div>
		</div>

		<div class="col-md-12">
			<div class="card card-body">
				<div class="container">
					<div class="" style="text-align:center">
						<h1>{{$galeria->titulo}}</h1>
						<span style="margin:10px 0px">{{$galeria->descricao}}</span>
					</div>
					<hr>

					<div class="">
						<div class="row">
							@foreach ($galeria->fotosGaleria as $foto)
								<div class="col-md-3">
									<div class="img">
										<img src="{{url('storage/'.$foto->nome)}}" alt="..." class="img-responsive" >
									</div>
									<div class="btnDelete" style="margin-top:6px; background: #f4f4f4;">
										@can ('admin_access')
											<a href="{{'deleteFoto/'.$foto->id}}">
												<button type="button" class="btn btn-danger" name="button">Excluir</button>
											</a>
										@endcan
									</div>
								</div>
							@endforeach

						</div>
					</div>

				</div>
			</div>
		</div>

	</div>

@endsection
