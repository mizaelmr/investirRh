@extends('layouts.sidebar')

@section('content')

	<div class="row">
		<div class="col-md-12">
			@if (session('msg-success'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg-success') }}
				</div>
			@endif

			@if (session('msg-error'))
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					{{ session('msg-error') }}
				</div>
			@endif
		</div>
	</div>


	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">

				<div class="row">
					<div class="col-md-12 just-end">
						<a href="{{url('/admin/createGalerias')}}">
							<button type="button" class="btn btn-info btn-lg">NOVO</button>
						</a>
					</div>
				</div>
				<div class="dropdown-divider"></div>

				<div style="margin-top:40px;">

					<table class="table table-striped" id="dataTables" >
						<thead>
							<tr>
								<th>Nome</th>
								<th>Descrição</th>
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							@isset($galerias)
							@foreach ($galerias as $value)
							<tr>
								<td>{{ $value->titulo }}</td>
								<td>{{ $value->descricao }}</td>
								<td>
									<a href="{{url('admin/showGaleria/'.$value->id)}}">
											<button type="button" class="btn btn-primary" name="button">Visualizar</button>
									</a>
									{{-- <button type="button" class="btn btn-warning" name="button">Editar</button> --}}
									<a href="{{url('admin/deleteGaleria/'.$value->id)}}">
										<button type="button" class="btn btn-danger" name="button">Excluir</button>
									</a>
								</td>
							</tr>
						@endforeach
						@endisset
						</tbody>
					</table>

				</div>

			</div>
		</div>
	</div>
@endsection
