@extends('layouts.sidebar')

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="card card-body">

			<div class="row">
				<div class="col-md-12">
					<form class="" action="{{url('/admin/storeGalerias')}}" method="post" enctype="multipart/form-data">
					<div class="form-group">
						<label for="">Titulo</label>
						<input type="text" class="form-control" name="titulo" value="">
						{{ csrf_field() }}
					</div>
					<div class="form-group">
						<label for="">Descrição</label>
						<textarea name="descricao" class="form-control" rows="8" cols="80"></textarea>
					</div>
					<div class="form-group">
						<label for="">Anexar Fotos</label>
						<input type="file" name="fotos[]" value="" multiple>
					</div>
					<div class="form-group" style="margin-top:30px;">
						<button type="submit" class="btn btn-success" name="button">Salvar</button>
					</div>
				</form>
				</div>
			</div>
			<div class="dropdown-divider"></div>

		</div>
	</div>
</div>

@endsection
