@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/home')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Candidatos</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="row">
					<div class="col-md-6">
						<h4>Lista de candidatos</h4>
						<span class="text-muted m-b-30 font-14">Número de candidatos: {{count($candidato)}}</span>
					</div>
				</div>
				<div class="dropdown-divider"></div>
				<div style="margin-top:40px;">

					<table class="table table-striped" id="dataTables" >
						<thead>
							<tr>
								<th>Nome</th>
								<th>E-mail</th>
								{{-- <th>Descrição</th> --}}
								<th>Membro desde</th>
								{{-- <th>Data</th> --}}
								<th>Ações</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($candidato as $value)

								<tr>
									<td>{{$value->name or " Não Possui dados."}}</td>
									<td>{{$value->email or " Não Possui dados."}}</td>
									{{-- <td>{!!$value->descricao or " Não Possui dados."!!}</td> --}}
									<td>@if (isset($value->created_at)) {{$value->created_at->format('d/m/Y')}}@endif</td>
									{{-- <td>{{$value->created_at->format('d/m/Y')}}</td> --}}
									<td>
										<a href="{{url('/admin/candidatoCurriculo')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Currículo" class="material-icons btn btn-info">&#xE873;</i></a>
										{{-- <a href="{{url('/')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Editar" class="material-icons btn btn-warning">&#xE8D7;</i></a> --}}
										{{-- <a href="{{url('/deleteProdutos')}}/{{$value->id}}"><i data-toggle="tooltip" data-placement="top" title="Excluir" class="material-icons btn btn-danger">&#xE92B;</i></a> --}}
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan

@endsection
