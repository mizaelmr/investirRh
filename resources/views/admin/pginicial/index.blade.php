@extends('layouts.sidebar')
@section('content')

@can ('admin_access')
	<div class="row">
		<div class="col-md-12">
			@if (session('msg'))
				<div class="alert alert-success alert-dismissible fade show" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	              <span aria-hidden="true">×</span>
	          </button>
					{{ session('msg') }}
				</div>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<div class="btn-group pull-right">
					<ol class="breadcrumb hide-phone p-0 m-0">
						<a href="{{url('/admin/home')}}">
							<li class="btn btn-info breadcrumb-item active">Voltar</li>
						</a>
					</ol>
				</div>
				<h4 class="page-title">Página Inicial</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="card card-body">
				<div class="row">
					<div class="col" style="margin-bottom: 50px; padding: 0px 20px;">
						<h1>
							<span>
								<span style="letter-spacing:-0.03em;">{{$dados[0]->titulo}}</span>
							</span>
						</h1>
						<p>{!!$dados[0]->descricao!!}</p>
						<hr>
						<a href="{{ url('/admin/EditPginicial')}}/{{$dados[0]->id}}" class="btn btn-primary waves-effect waves-light">Editar</a>
						<small class="text-muted">Atualizado pelo administrador <b>{{$dados[0]->usuario->name}}</b></small>
					</div>
				</div>
			</div>
		</div>
	</div>
@endcan

@can ('client_access')
	<div class="wrapper-page">

		<div class="card">
			<div class="card-block">

				<div class="ex-page-content text-center">
					<h1 class="">404!</h1>
					<h3 class="">Página não encontrada!</h3><br>

					<a class="btn btn-info mb-5 waves-effect waves-light" href="{{url('/')}}">Voltar!</a>
				</div>

			</div>
		</div>
	</div>
@endcan


@endsection
