<link href="css/admin/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/admin/style.css" rel="stylesheet" type="text/css">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<body style="overflow: visible;">

        <!-- Begin page -->
        <div class="accountbg"></div>
        <div class="wrapper-page">

            <div class="card">
                <div class="card-body">

                    <h3 class="text-center mt-0 m-b-15">
                        <a href="{{url('/')}}" class="logo logo-admin"><img src="images/logo_site.png" height="130" alt="logo"></a>
                    </h3>

                    {{-- <h4 class="text-muted text-center font-18"><b>Sign In</b></h4> --}}

                    <div class="p-3">
											<form class="form-horizontal" method="POST" action="{{ route('login') }}">
	                        {{ csrf_field() }}

                            <div class="form-group row">
                                <div class="col-12 {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" class="form-control" type="text" name="email" required="" placeholder="E-mail">
																		@if ($errors->has('email'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('email') }}</strong>
		                                    </span>
		                                @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12 {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" class="form-control" name="password" placeholder="Senha" required>
																		@if ($errors->has('password'))
		                                    <span class="help-block">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" name="remember" {{ old('remember') ? 'checked' : '' }} id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">lembre-me</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group text-center row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-info btn-block waves-effect waves-light" type="submit">Entrar</button>
                                </div>
																{{---<div class="col-12" style="margin-top:10px">
                                  <a href="{{url('/redirect')}}" class="btn-block btn btn-primary">Login com o Facebook</a>
                                </div>---}}
                            </div>

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-sm-7 m-t-20">
                                    <a href="{{ route('password.request') }}" class="text-muted"><i class="mdi mdi-lock"></i> Esqueceu sua senha?</a>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>




</body>
