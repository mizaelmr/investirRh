@extends('layouts.sidebar')
@section('content')

	<div class="row">
		<div class="col-sm-12">
			<div class="page-title-box">
				<h4 class="page-title">Dashboard</h4>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			@can ('admin_access')
				<div class="">
					<div class="row">
						<div class="col-md-6 col-xl-3">
							<div class="mini-stat clearfix bg-white">
								<span class="mini-stat-icon bg-light">
									<i class="material-icons">supervisor_account</i></span>
									<div class="mini-stat-info text-right text-muted">
										<span class="counter text-danger">{{$countU or " "}}</span>
										Total Usuários
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xl-3">
								<div class="mini-stat clearfix bg-success">
									<span class="mini-stat-icon bg-light">
										<i class="material-icons">build</i></span>
										<div class="mini-stat-info text-right text-white">
											<span class="counter text-white">{{$countV or " "}}</span>
											Vagas Cadastradas
										</div>
									</div>
								</div>
								<div class="col-md-6 col-xl-3">
									<div class="mini-stat clearfix bg-white">
										<span class="mini-stat-icon bg-light">
											<i class="material-icons">description</i></span>
											<div class="mini-stat-info text-right text-muted">
												<span class="counter text-warning">{{$countB or " =(  "}}</span>
												Post publicados
											</div>
										</div>
									</div>
									<div class="col-md-6 col-xl-3">
										<div class="mini-stat clearfix bg-info">
											<span class="mini-stat-icon bg-light">
												<i class="material-icons">mail</i></span>
												<div class="mini-stat-info text-right text-light">
													<span class="counter text-white">{{$countM or " "}}</span>
													Email a responder
												</div>
											</div>
										</div>
									</div>
									<div class="row">

										<div class="col-xl-8">
											<div class="card m-b-30">
												<div class="card-body">
													<h4 class="mt-0 m-b-15 header-title">Ultimos Post</h4>
													<hr>
													<div class="table-responsive">
														<table class="table table-hover mb-0" id="dataTables">
															<thead>
																<tr>
																	<th>Título</th>
																	<th>Autor</th>
																	<th>Data</th>
																	<th>Hora</th>
																	<th>Editar</th>
																</tr>
															</thead>
															<tbody>
																@foreach ($blogHome as $value)
																<tr>
																	<td>{{$value->titulo}}</td>
																	<td>{{$value->users_id}}</td>
																	<td>{{$value->created_at}}</td>
																	<td>{{$value->created_at}}</td>
																	<td><button type="button" class="btn btn-warning">Editar</button></td>
																</tr>
															@endforeach
															</tbody>
														</table>
													</div>

												</div>
											</div>
										</div>

										<div class="col-xl-4">
											<div class="card m-b-30">
												<div class="card-body">
													<h4 class="mt-0 header-title">Comentários para aprovar</h4>
														<table class="table table-hover mb-0" id="dataTables">
															<thead>
																<tr>
																	<td>Nome:</td>
																	<td>Data:</td>
																	<td>Ações:</td>
																</tr>
															</thead>
															<tbody>
																@foreach ($comentarios as $coments)
																<tr>
																	<td>{{$coments->nome}}</td>
																	<td>{{$coments->created_at->format('d/m/Y - H:m')}}</td>
																	<td>
																		<a href="{{url('/admin/showComentario')}}/{{$coments->id}}"><i data-toggle="tooltip" data-placement="top" title="Detalhes" class="material-icons btn btn-info">&#xE873;</i></a>
																		{{-- <a href="#"> <button type="button" class="btn btn-success"></button></a>
																		<a href="#"> <button type="button" class="btn btn-danger"></button></a> --}}
																	</td>
																</tr>
															@endforeach
															</tbody>
														</table>
												</div>
											</div>

										</div>
									</div>{{--Dashboard Admin--}}
								@endcan

								@can ('client_access')
									<div class="card card-body" style="margin-bottom:60px;">
										<div class="">
											<div class="" style="padding-left:20px;">
												<h3 style="text-transform: uppercase">Olá {{$user->name}}</h3>
												<p>Por favor, preencha todos os campos em <b>Editar Currículo</b>, para poder se candidatar nas vagas disponíveis</p>
											</div>
											<div class="col-md-12 col-xs-12">
													<hr>
													<div class="row" style="margin:40px 10px;">
														<div class="col">
															<h2 class="ClassCenter">
																<i class="material-icons titleIcon">&#xE873;</i>
																<a href="{{url('/admin/dados')}}">
																	Editar Currículo
																</a>
															</h2>
														</div>
														<div class="col">
															<h2 class="ClassCenter">
																<i class="material-icons titleIcon">&#xE8F8;</i>
																<a href="{{url('/admin/vagas')}}">
																	Vagas
																</a>
															</h2>
														</div>
														<div class="col">
															<h2 class="ClassCenter">
																<i class="material-icons titleIcon">&#xE880;</i>
																<a href="{{url('/admin/historico')}}/{{$user->id}}">
																	Histórico
																</a>
															</h2>
														</div>
													</div>
													<hr>
													<div class="">
														<div class="">
															<h2 class="page-title" style="text-transform: uppercase; padding:20px 20px 30px 0px;">Últimas Notícias do nosso blog</h2>
														</div>
														<div class="owl-carousel">
															@foreach ($dados as $dado)
																<div>
																	<div class="card m-b-30">
																		@isset($dado->foto)
																			<img class="card-img-top img-fluid" src="{{url('/storage')}}/{{$dado->foto}}" alt="Card image cap" style="max-height:200px; width:100%">
																		@else
																			<img clas="img-responsive" src="{{url('images/investir_default.jpg')}}" alt="">
																		@endisset
																		<div class="card-body">
																			<h4 class="card-title font-20 mt-0">{{$dado->titulo}}</h4>
																			<p class="card-text">{!! substr($dado->descricao,0,150) !!}...</p>
																			<p><a href="{{url('/detalhesblog')}}/{{$dado->id}}" target="_blank">
																				<button type="button" class="btn btn-primary">
																					Leia Mais
																				</button></a></p>
																				<p class="card-text">
																					<small class="text-muted">Data: {{$dado->created_at}}</small>
																				</p>
																			</div>
																		</div>
																	</div>
																@endforeach

															</div>
														</div>
													</div>
												</div>
											</div>{{--Dashboard cliente--}}
										@endcan
									</div>
								</div>


							@endsection
