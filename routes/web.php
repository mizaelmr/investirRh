<?php



// Route::get('/', function () {return view('Site.inicial');});
// Route::get('/email', function () {return view('admin.candidatura.email');});
Route::get('/email', function () {return view('admin.contato.email');});
Route::get('/', 'Site\SiteIndexController@index');
Route::get('/empresa', 'Site\SiteEmpresaController@index');
Route::get('/vagas', 'Site\SiteVagasController@index');
Route::get('/detalhesvagas/{id}', 'Site\SiteVagasController@detalhes');
Route::get('/blog', 'Site\SiteBlogController@index');
Route::get('/detalhesblog/{id}', 'Site\SiteBlogController@detalhes');
Route::get('/produtos', 'Site\SiteProdutoController@index');
Route::get('/detalhesproduto/{id}', 'Site\SiteProdutoController@detalhes');
Route::get('/cursos', 'Site\SiteCursoController@index');
Route::get('/detalhescurso/{id}', 'Site\SiteCursoController@detalhes');
Route::get('/site/contato', 'Site\SiteContatoController@index');
Route::post('/site/formcontato', 'Site\SiteContatoController@form');
Route::get('/buscar', 'Site\SiteVagasController@buscar');
Route::post('/storeComentario/{id}','Site\SiteComentarioController@store');
Route::get('/galerias', 'Site\SiteGaleriasController@index');
Route::get('/detalhegalerias/{id}', 'Site\SiteGaleriasController@show');
// /Site

// Route::get('/redirect', 'SocialAuthFacebookController@redirect');
// Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
Route::get('/home', 'HomeController@index');

// VAGAS
Route::get('/vagas', 'Admin\VagasController@index');
Route::get('/showVagas/{id}', 'Admin\VagasController@show');
Route::get('/editVagas/{id}', 'Admin\VagasController@edit');
Route::post('/updateVagas/{id}', 'Admin\VagasController@update');
Route::get('/createVagas', 'Admin\VagasController@create');
Route::post('/storeVagas', 'Admin\VagasController@store');
Route::get('/deleteVagas/{id}', 'Admin\VagasController@delete');
Route::get('/candidatarVaga/{id}', 'Admin\VagasController@candidatar');
Route::get('/ativoVagas/{id}', 'Admin\VagasController@ativo');

//EMPRESA
Route::get('/empresa', 'Admin\EmpresaController@index');
Route::get('/EditEmpresa/{id}','Admin\EmpresaController@empresa');
Route::post('/updateEmpresa/{id}','Admin\EmpresaController@update');

//CONTATO
Route::get('/contato', 'Admin\ContatoController@index');
Route::get('/AnswerContato/{id}','Admin\ContatoController@contato');
Route::post('/submitResposta/{id}', 'Admin\ContatoController@email' );
Route::get('/respostaContato/{id}','Admin\ContatoController@contato');
Route::post('/submitResposta/{id}', 'Admin\ContatoController@email');

//PRODUTOS
Route::get('/produtos', 'Admin\ProdutosController@index');
Route::get('/createProdutos', 'Admin\ProdutosController@create');
Route::get('/editProdutos/{id}', 'Admin\ProdutosController@edit');
Route::post('/updateProdutos/{id}', 'Admin\ProdutosController@update');
Route::post('/storeProdutos', 'Admin\ProdutosController@store');
Route::get('/deleteProdutos/{id}', 'Admin\ProdutosController@delete');
Route::get('/showProdutos/{id}', 'Admin\ProdutosController@show');

//CURSOS
Route::get('/cursos', 'Admin\CursoController@index');
Route::get('/createCurso', 'Admin\CursoController@create');
Route::post('/storeCurso', 'Admin\CursoController@store');
Route::get('/deleteCurso/{id}', 'Admin\CursoController@delete');
Route::get('/showCurso/{id}', 'Admin\CursoController@show');
Route::get('/editCurso/{id}', 'Admin\CursoController@edit');
Route::post('/updateCursos/{id}', 'Admin\CursoController@update');

//CLIENTES
Route::get('/clientes', 'Admin\ClientesController@index');
Route::get('/createClientes', 'Admin\ClientesController@create');
Route::post('/storeClientes', 'Admin\ClientesController@store');
Route::get('/deleteClientes/{id}', 'Admin\ClientesController@delete');

//PARCEIROS
Route::get('/parceiros', 'Admin\ParceirosController@index');
Route::get('/createParceiros', 'Admin\ParceirosController@create');
Route::post('/storeParceiros', 'Admin\ParceirosController@store');
Route::get('/deleteParceiros/{id}', 'Admin\ParceirosController@delete');

//BLOG
Route::get('/blog', 'Admin\BlogController@index');
Route::get('/createBlog', 'Admin\BlogController@create');
Route::post('/storeBlog', 'Admin\BlogController@store');
Route::get('/deleteBlog/{id}', 'Admin\BlogController@delete');
Route::get('/editBlog/{id}', 'Admin\BlogController@edit');
Route::post('/updateBlog/{id}', 'Admin\BlogController@update');
Route::get('/showBlog/{id}', 'Admin\BlogController@show');


//PGINICIAL
Route::get('/pginicial', 'Admin\PginicialController@index');
Route::get('/EditPginicial/{id}','Admin\PginicialController@pginicial');
Route::post('/updatePginicial/{id}','Admin\PginicialController@update');

//SLIDE
Route::get('/slides', 'Admin\SlideController@index');
Route::get('/createSlide', 'Admin\SlideController@create');
Route::post('/storeSlide', 'Admin\SlideController@store');
Route::get('/deleteSlide/{id}', 'Admin\SlideController@delete');

//DADOS
Route::get('/dados', 'Admin\DadosCadastraisController@index');
Route::get('/editDados','Admin\DadosCadastraisController@dados');
Route::post('/updateDados/{id}','Admin\DadosCadastraisController@update');

//EXPERIENCIA
Route::get('/createExperiencia','Admin\ExperienciaController@create');
Route::post('/storeExperiencia', 'Admin\ExperienciaController@store');
Route::post('/updateExperiencia/{id}','Admin\ExperienciaController@update');
Route::get('/editExperiencia/{id}','Admin\ExperienciaController@edit');
Route::get('/deleteExperiencia/{id}','Admin\ExperienciaController@delete');

//CURRICULO
Route::get('/curriculo', 'Admin\CurriculoController@index');
Route::get('/editCurriculo/{id}','Admin\CurriculoController@curriculo');
Route::post('/updateCurriculo/{id}','Admin\CurriculoController@update');

//ENSINO
Route::get('/createEnsino', 'Admin\EnsinoController@create');
Route::post('/storeEnsino', 'Admin\EnsinoController@store');
Route::get('/editEnsino/{id}','Admin\EnsinoController@edit');
Route::post('/updateEnsino/{id}','Admin\EnsinoController@update');
Route::get('/deleteEnsino/{id}','Admin\EnsinoController@delete');

//HISTÓRICO
Route::get('/historico/{id}', 'Admin\CandidaturaController@historico');

//CANDIDATURA
Route::get('/candidaturas', 'Admin\CandidaturaController@index');

//CANDIDATOS
Route::get('/candidatos', 'Admin\CandidatoController@index');
Route::get('/candidatoCurriculo/{id}', 'Admin\CandidatoController@curriculo');
Route::get('/testecurriculo/{id}', 'Admin\CandidatoController@curriculoPdf'); //PDF

//MENU
Route::get('/contas', 'Admin\MenuController@conta');
Route::get('/createAdmin', 'Admin\MenuController@createConta');
Route::post('/newUser', 'Admin\MenuController@newUser');
Route::get('/deleteUser/{id}', 'Admin\MenuController@deleteUser');
Route::get('/categorias', 'Admin\MenuController@categoria');
Route::get('/createCateg', 'Admin\MenuController@createCateg');
Route::post('/storeCateg', 'Admin\MenuController@storeCateg');
Route::get('/deleteCateg/{id}', 'Admin\MenuController@deleteCateg');

//COMENTARIOS
Route::get('/showComentario/{id}','Admin\ComentarioController@show');
Route::get('/aprovarComentario/{id}','Admin\ComentarioController@aprovar');
Route::get('/deleteComentario/{id}','Admin\ComentarioController@delete');

//Galerias
Route::get('/galerias','Admin\GaleriasController@index');
Route::get('/showGaleria/{id}','Admin\GaleriasController@show');
Route::get('/createGalerias','Admin\GaleriasController@create');
Route::post('/storeGalerias','Admin\GaleriasController@store');
Route::get('/editGalerias','Admin\GaleriasController@edit');
Route::post('/updateGalerias','Admin\GaleriasController@update');
Route::get('/deleteGaleria/{id}','Admin\GaleriasController@delete');
Route::get('showGaleria/deleteFoto/{id}', 'Admin\GaleriasController@deleteFoto');

});
Auth::routes();
