<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVagasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vagas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
						$table->string('empresa')->nullable();
						$table->string('num_vagas')->nullable();
            $table->string('atribuicoes')->nullable();
            $table->string('experiencia');
						$table->string('beneficios')->nullable();
						$table->longText('descricao')->nullabel();
            $table->string('formacao');
						$table->string('sexo')->nullable();
            $table->string('local')->nullable();
						$table->integer('ativo')->nullable();
						$table->integer('categorias_id')->unsigned();
						$table->foreign('categorias_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vagas');
    }
}
