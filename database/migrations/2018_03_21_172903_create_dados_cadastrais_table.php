<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDadosCadastraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dados_cadastrais', function (Blueprint $table) {
					$table->increments('id');
					$table->string('foto')->nullable();
					$table->string('nomeComp');
					$table->date('nascimento');
					$table->string('atuacao');
					$table->string('cnh');
					$table->string('rua');
					$table->integer('numero')->nullable();
					$table->string('cep');
					$table->string('bairro');
					$table->string('cidade');
					$table->string('estado');
					$table->string('instagram')->nullable();
					$table->string('facebook')->nullable();
					$table->string('tel')->nullable();
					$table->string('cel');
					$table->integer('users_id')->unsigned();
					$table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
					$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dados_cadastrais');
    }
}
