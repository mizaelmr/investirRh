<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
					$table->increments('id');
					$table->string('nome');
					$table->longText('email');
					$table->longText('comentario');
					$table->integer('ativo');
					$table->integer('blog_id')->unsigned();
					$table->foreign('blog_id')->references('id')->on('blogs')->onDelete('cascade');			
					$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comentarios');
    }
}
