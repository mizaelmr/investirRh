<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
					$table->increments('id');
					$table->string('assunto')->nullable();;
					$table->string('nome')->nullable();
					$table->string('email')->nullable();
					$table->longtext('descricao')->nullable();
					$table->date('data')->nullable();
					$table->integer('status')->nullable();
					$table->longText('resposta')->nullable();
					// $table->integer('users_id')->unsigned();
					// $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
					$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contato');
    }
}
