<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Candidaturas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('candidaturas', function (Blueprint $table) {
          $table->increments('id');
          $table->integer('users_id')->unsigned();
          $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
          $table->integer('vagas_id')->unsigned();
          $table->foreign('vagas_id')->references('id')->on('vagas')->onDelete('cascade');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
