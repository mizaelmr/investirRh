<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'administrador',
          'email' => 'admin@gmail.com',
          'password' => bcrypt('admin'),
          'tipo_users' => 1,
      ]);
    }
}
