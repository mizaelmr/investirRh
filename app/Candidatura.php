<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Candidatura extends Model
{
	protected $fillable = [
			'id', 'users_id', 'vagas_id,'
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}

	public function vaga(){
		return $this->belongsTo('App\Vaga', 'vagas_id');
	}

	protected $table = 'candidaturas';

}
