<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ensino extends Model
{
	protected $fillable = [
			'id', 'instituicao', 'curso', 'descricao', 'dataInic', 'dataFim', 'users_id',
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}
}
