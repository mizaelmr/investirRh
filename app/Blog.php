<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
	protected $fillable = [
			'id', 'titulo', 'descricao', 'users_id',
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}
}
