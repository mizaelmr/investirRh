<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
			protected $fillable = [
				'nome',
				'capa',
				'galeria_id',
			];
}
