<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parceiros extends Model
{
	protected $fillable = [
			'id','nome','contato','foto', 'descricao',
	];
}
