<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
	protected $fillable = [
			'id','nome','arquivo', 'descricao',
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}

	protected $table = "cursos";
}
