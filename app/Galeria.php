<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Galeria extends Model
{
    protected $fillable = [
			'titulo',
			'descricao',
		];

		public function fotosGaleria(){
			return $this->HasMany('App\Foto');
		}
}
