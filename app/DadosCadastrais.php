<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DadosCadastrais extends Model
{
	protected $fillable = [
			'foto',
			'nomeComp',
			'nascimento',
			'cnh',
			'rua',
			'numero',
			'cep',
			'bairro',
			'cidade',
			'estado',		
			'tel',
			'cel',
			'users_id',
			'atuacao',
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}



	// protected $dates = ['nascimento'];
}
