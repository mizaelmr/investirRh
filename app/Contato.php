<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
	protected $fillable = [
			'id','assunto','nome','email','descricao','data','status', 'resposta',
	];

	protected $table = 'contato';
}
