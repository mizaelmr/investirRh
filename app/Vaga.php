<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaga extends Model
{
	protected $fillable = [
			'id','titulo','num_vagas', 'formacao', 'experiencia','beneficios','descricao',
			'horario','necessidades','sexo','local','categorias_id', 'empresa', 'ativo',
	];


	public function categoria(){
		return $this->belongsTo('App\Categorias', 'categorias_id');
	}

	public function CandidaturaS(){
		return $this->belongsTo('App\Candidatura', 'users_id');
	}
}
