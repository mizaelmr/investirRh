<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
	protected $fillable = [
			'id','nome','foto','users_id',
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}
}
