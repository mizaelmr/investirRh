<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experiencia extends Model
{
	protected $fillable = [
			'id', 'titulo', 'empresa', 'periodo', 'cidade', 'estado', 'descricao', 'users_id',
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}
}
