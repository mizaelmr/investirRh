<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculo extends Model
{
	protected $fillable = [
			'id', 'descricao', 'users_id'
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}
}
