<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VagasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
		 public function rules()
     {
         return [
 					'titulo' => 'required',
 					'formacao' => 'required',
					'experiencia' => 'required',
					// 'descricao' => 'required|min:20',
					'categorias_id' => 'required',
					'num_vagas' => 'required',
         ];
     }

 		public function messages()
 		{
 		    return [
 		        'titulo.required' => 'O título é obrigatório!',
						'formacao.required' => 'A formação é obrigatória!',
						'num_vagas.required' => 'Número de vagas é obrigatório!',
						'experiencia.required' => 'A experiência é obrigatória!',
						// 'descricao.required' => 'O texto é obrigatório!',
 						'categorias_id.required' => 'Selecione um estilo de contrato!',
 		        // 'descricao.min' => 'Digite algo no campo descrição!',
 		    ];
 		}
}
