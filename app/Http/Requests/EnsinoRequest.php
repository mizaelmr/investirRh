<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EnsinoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
		 public function rules()
     {
         return [
 					'instituicao' => 'required',
 					'curso' => 'required',
					// 'descricao' => 'required',
 					'dataInic' => 'required',
					'dataFim' => 'required',
         ];
     }

 		public function messages()
 		{
 		    return [
 		        'instituicao.required' => 'A instituição é obrigatória!',
 						'curso.required' => 'O curso é obrigatório!',
 		        // 'descricao.required' => 'A descrição é obrigatória!',
						'dataInic.required' => 'A data inicial é obrigatória!',
						'dataFim.required' => 'A data final é obrigatória!',
 		    ];
 		}
}
