<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DadosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
					'email' => 'required',
					'cep' => 'required',
					'rua' => 'required|string',
					'cep' => 'required',
					'bairro' => 'required|string',
					'cidade' => 'required|string',
					'estado' => 'required',
					'cnh' => 'required',
					//'atuacao' => 'required',
        ];
    }
		public function messages()
		{
		    return [
		        'email.required' => 'O email é obrigatório!',
						'cep.required' => 'O CEP é obrigatório!',
		        'rua.required' => 'A rua é obrigatória!',
						'bairro.required' => 'O bairro é obrigatório!',
						'cidade.required' => 'A cidade é obrigatória!',
						'estado.required' => 'O estado é obrigatório!',
						'cnh.required' => 'A CNH é obrigatória!',
						//'atuacao.required' => 'A atuação é obrigatória!',
		    ];
		}
}
