<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExperienciaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
		 public function rules()
     {
         return [
 					'titulo' => 'required',
 					'empresa' => 'required',
					'periodo' => 'required',
 					'cidade' => 'required',
					'estado' => 'required',
					// 'descricao' => 'required',
         ];
     }

 		public function messages()
 		{
 		    return [
 		        'empresa.required' => 'A empresa é obrigatória!',
 						'titulo.required' => 'O título é obrigatório!',
						'periodo.required' => 'O período é obrigatório!',
						'estado.required' => 'O estado é obrigatório!',
						'cidade.required' => 'A cidade é obrigatória!',
 		        // 'descricao.required' => 'A descrição é obrigatória!',
 		    ];
 		}
}
