<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
		 public function rules()
 		{
 				return [
 					'titulo' => 'required',
 					'descricao' => 'required|min:20',
 				];
 		}

 		public function messages()
 		{
 				return [
 						'titulo.required' => 'O título é obrigatório!',
 						'descricao.required' => 'A descrição é obrigatória!',
						'descricao.min' => 'Digite algo no campo descrição!',
 				];
 		}
}
