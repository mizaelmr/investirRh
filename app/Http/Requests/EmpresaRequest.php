<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmpresaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    } 

    public function rules()
    {
        return [
					'titulo' => 'required',
					'descricao' => 'required|min:20',
        ];
    }

		public function messages()
		{
		    return [
		        'titulo.required' => 'O título é obrigatório!',
						'descricao.required' => 'O texto é obrigatório!',
		        'descricao.min' => 'Digite algo no campo descrição!',
		    ];
		}
}
