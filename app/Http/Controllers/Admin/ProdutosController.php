<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ProdutosRequest;
use App\Produtos;
use Session;

class ProdutosController extends Controller
{
	public function index(){
		$dados = Produtos::all();
		//dd($dados);
		return view('admin.produtos.index',compact('dados'));
	}

	public function create(){
		return view('admin.produtos.create');
	}

	public function store(ProdutosRequest $request){
		$store = new Produtos();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		//dd($request->hasFile('arquivo'));
		if($request->hasFile('arquivo')){
			$file = $request->file('arquivo');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->arquivo->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$store->arquivo = $fileName;
			$store->save();
			Session::flash('msg', 'Cadastrado com sucesso');
			return redirect('/admin/produtos');
		}else{
			Session::flash('msg', 'Erro ao cadastrar');
			return redirect('/admin/produtos');
		}
	}


	public function delete($id){
		$arquivos = Produtos::find($id);
		//dd($arquivos->arquivo);
		if($arquivos->arquivo != null){
			Storage::delete("public/$arquivos->arquivo");
		}
		$arquivos->delete();
		Session::flash('msg', 'Deletado com sucesso');
		return redirect('/admin/produtos');
	}

		public function show($id){
			$produtos = Produtos::find($id);
			return view('admin.produtos.show', compact('produtos'));
		}

		public function edit($id){
			$dados = Produtos::find($id);
			//dd($dados);
			return view('admin.produtos.edit',compact('dados'));
		}

		public function update(ProdutosRequest $request, $id){
			$produto = Produtos::find($id);
			//dd($produto);
			$usuario_id = Auth()->user()->id;
			$produto->fill($request->all());
			$produto->users_id = $usuario_id;
			if($request->hasFile('arquivo')){
				$file = $request->file('arquivo');
				$fileName = rand(1, 999) . $file->getClientOriginalName();
				$request->arquivo->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
				$produto->arquivo = $fileName;
			}
			$produto->save();
			Session::flash('msg', 'Cadastrado com sucesso');
			return redirect('/admin/produtos');
		}
}
