<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Mail\ConfirmaCandidatura;
use App\Ensino;
Use App\Experiencia;
use App\Candidato;
use App\Candidatura;
use App\Categorias;
use App\DadosCadastrais;
use App\Vaga;
use App\User;
use Session;
use Auth;
use DB;
use App\Http\Requests\VagasRequest;

class VagasController extends Controller
{
    public function index(){
			$user = Auth::User()->id;
			$user = User::find($user);
			if($user->tipo_users == 1){
				$dados = Vaga::orderBy('titulo', 'asc')->get();
			}else{
				$dados = Vaga::where('ativo', 1)->orderBy('titulo', 'asc')->get();
			}
			$cand = Candidatura::all();
			return view('admin.vagas.index', compact('dados', 'cand', 'user'));
		}

		public function show($id){
		$vaga = Vaga::find($id);
		$candidatura = Candidatura::where('vagas_id',$id)->get();

		$dados = DB::table('dados_cadastrais')->join('candidaturas',  'candidaturas.users_id', '=', 'dados_cadastrais.users_id')
            ->select('dados_cadastrais.*')
            ->get();
		return view('admin.vagas.show', compact('vaga','candidatura', 'dados'));
		}

		public function create(){
			$categorias = Categorias::all();
			return view('admin.vagas.create', compact('categorias'));
		}

		public function store(VagasRequest $request){
			$store = new Vaga();
			$store->fill($request->all());
			$store->ativo = 1;
			$store->save();
			Session::flash('msg', 'Suas definições foram atualizadas com Sucesso!');
      return redirect('/admin/vagas');
		}

		public function edit($id){
		$vaga = Vaga::find($id);
		$categorias = Categorias::all();
		return view('admin.vagas.edit', compact('vaga','categorias'));
		}

		public function update(VagasRequest $request, $id){
		$vaga = Vaga::find($id);
		$vaga->fill($request->all());
		$vaga->save();
		return redirect('/admin/vagas');
		}

		public function delete($id){
		$vaga = Vaga::find($id);
		//dd($vaga);
		$vaga->delete();
		Session::flash('msg', 'Vaga deletada com Sucesso!');
		return redirect('/admin/vagas');
		}

		public function ativo($id){
		$vaga = Vaga::find($id);
		if ($vaga->ativo == 0) {
			$vaga->ativo = 1;
			Session::flash('msg', 'Vaga ativada com Sucesso!');
		}
		else {
			$vaga->ativo = 0;
			Session::flash('msg', 'Vaga desativada com Sucesso!');
		}
		//dd($vaga);
		$vaga->save();
		return redirect('/admin/vagas');
		}

		public function candidatar ($id){
			$cand = new Candidatura();
			$user = Auth::User();

			$ensino = Ensino::where('users_id',$user->id)->get();
			$experiencia = Experiencia::where('users_id',$user->id)->get();
			$dadosCadastrais = DadosCadastrais::where('users_id',$user->id)->get();


			if ( (count($ensino) != 0) && (count($experiencia) != 0) && (count($dadosCadastrais) != 0) ) {
				$cand->vagas_id = $id;
				$cand->users_id = $user->id;
				$cand->save();

				Mail::to($user->email)->send(new ConfirmaCandidatura($user));

				Session::flash('msg', 'Candidatura realizada com Sucesso!');
				return redirect('/admin/vagas');
			}else{
				if (count($ensino) == 0) {
					Session::flash('msg1', 'Você não possui formação cadastrada, por favor atualize seu currículo!');
				}
				if (count($experiencia) == 0) {
					Session::flash('msg2', 'Você não possui experiência cadastrada, por favor atualize seu currículo!');
				}
				if (count($dadosCadastrais) == 0) {
					Session::flash('msg3', 'Seus dados cadastrais estão incompletos, por favor atualize seu currículo!');
				}
				return redirect('/admin/vagas');
			}
		}

}
