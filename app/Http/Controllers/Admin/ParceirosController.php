<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ParceirosRequest;
use App\Parceiros;
use Session;

class ParceirosController extends Controller
{
	public function index(){
		$dados = Parceiros::all();
		//dd($dados);
		return view('admin.parceiros.index',compact('dados'));
	}

	public function create(){
		return view('admin.parceiros.create');
	}

	public function store(ParceirosRequest $request){
		$store = new Parceiros();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		//dd($file = $request->file('foto'));
		if($request->hasFile('foto')){
			$file = $request->file('foto');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->foto->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$store->foto = $fileName;
			$store->save();
			Session::flash('msg', 'Cadastrado com sucesso');
			return redirect('/admin/parceiros');
		}else{
			Session::flash('msg', 'Erro ao cadastrar');
			return redirect('/admin/parceiros');
		}
	}

	public function delete($id){
      $arquivos = Parceiros::find($id);
      // echo "public/$arquivos->arquivo";
      if( Storage::delete("public/$arquivos->foto")){
        $arquivos->delete();
        Session::flash('msg', 'Deletado com sucesso');
        return redirect('/admin/parceiros');
        }else{
          Session::flash('msg', 'Erro ao deletar arquivo');
          return redirect('/admin/parceiros');
        }
    }
}
