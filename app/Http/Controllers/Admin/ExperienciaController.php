<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\ExperienciaRequest;
use Illuminate\Http\Request;
use App\Experiencia;
use Session;
use Gate;

class ExperienciaController extends Controller
{
	public function create(){
		$cara = Auth()->user()->id;
		//$user = User::find($cara);
		//$dados = Experiencia::where('users_id', $cara)->first();
		//dd($cara);
		return view('admin.dados.experiencia.create', compact('cara'));
	}

	public function store(ExperienciaRequest $request){
		$store = new Experiencia();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		//dd($store);
		$store->save();
		Session::flash('msg', 'Cadastrado com sucesso');
		return redirect('/admin/dados');
	}

	public function edit($id){
		$dados = Experiencia::find($id);
		//dd($experiencia);
		if( Gate::denies('user_dados', $dados)){
			// abort(403, 'Não autorizado');
			return redirect('/admin/dados');
		}
		return view('admin.dados.experiencia.edit', compact('dados'));
	}

	public function update(ExperienciaRequest $request, $id){
		$texto = Experiencia::find($id);
		$dados = new Experiencia();

		if( Gate::denies('user_dados', $texto)){
			// abort(403, 'Não autorizado');
			return redirect('/admin/dados');
		}

		$dados->fill($request->all());
		$texto->titulo = $dados->titulo;
		$texto->empresa = $dados->empresa;
		$texto->periodo = $dados->periodo;
		$texto->cidade = $dados->cidade;
		$texto->estado = $dados->estado;
		$texto->descricao = $dados->descricao;

		// dd($texto);
		$texto->save();
		Session::flash('msg', 'Edição atualizada com Sucesso!');
		return redirect('/admin/dados');
		// return "Cadastrando..";
	}

	public function delete($id){
		$exp = Experiencia::find($id);
		$exp->delete();
		Session::flash('msg', 'Experiencia excluida com Sucesso!');
		return redirect('/admin/dados');
	}
}
