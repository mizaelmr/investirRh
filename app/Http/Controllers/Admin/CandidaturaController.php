<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Candidatura;

class CandidaturaController extends Controller
{
	public function historico($id){
		$candidatura = Candidatura::where('users_id', $id)->orderBy('created_at', 'desc')->get();
		//dd($candidatura);
		return view('admin.candidatura.historico',compact('candidatura'));
	}

	public function index(){
		$candidatura = Candidatura::orderBy('created_at', 'desc')->get();
		//dd($candidatura);
		return view('admin.candidatura.index',compact('candidatura'));
	}
}
