<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Slide;
use Session;
class SlideController extends Controller
{
    public function index(){
			$dados = Slide::all();
      //dd($dados);
      return view('admin.slide.index', compact('dados'));
		}

		public function create(){
			return view('admin.slide.create');
		}

		public function store(Request $request){
			$store = new Slide();
			$usuario_id = Auth()->user()->id;
			$store->fill($request->all());
			$store->users_id = $usuario_id;
			//dd($request->hasFile('foto'));
			if($request->hasFile('foto')){
				$file = $request->file('foto');
				$fileName = rand(1, 999) . $file->getClientOriginalName();
				$request->foto->storeAs('public', $fileName ); // move o foto para ( storage / public)
				$store->foto = $fileName;
				$store->save();
				Session::flash('msg', 'Cadastrado com sucesso');
				return redirect('/admin/slides');
			}else{
				Session::flash('msg', 'Erro ao cadastrar');
				return redirect('/admin/slides');
			}
		}

		public function delete($id){
			$arquivos = Slide::find($id);
			//dd($arquivos->arquivo);
			if($arquivos->arquivo != null){
				Storage::delete("public/$arquivos->arquivo");
			}
			$arquivos->delete();
			Session::flash('msg', 'Deletado com sucesso');
			return redirect('/admin/slides');
		}
}
