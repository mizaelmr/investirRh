<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ClienteRequest;
use App\Clientes;
use Session;

class ClientesController extends Controller
{
	public function index(){
		$dados = Clientes::all();
		//dd($dados);
		return view('admin.clientes.index',compact('dados'));
	}

	public function create(){
		return view('admin.clientes.create');
	}

	public function store(ClienteRequest $request){
		$store = new Clientes();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		// dd($file = $request->file('foto'));
		if($request->hasFile('foto')){
			$file = $request->file('foto');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->foto->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$store->foto = $fileName;
			$store->save();
			Session::flash('msg', 'Cadastrado com sucesso');
			return redirect('/admin/clientes');
		}else{
			Session::flash('msg', 'Erro ao cadastrar');
			return redirect('/admin/clientes');
		}
	}

	public function delete($id){
      $arquivos = Clientes::find($id);
      if( Storage::delete("public/$arquivos->foto")){
        $arquivos->delete();
        Session::flash('msg', 'Deletado com sucesso');
        return redirect('/admin/clientes');
        }else{
          Session::flash('msg', 'Erro ao deletar arquivo');
          return redirect('/admin/clientes');
        }
    }


}
