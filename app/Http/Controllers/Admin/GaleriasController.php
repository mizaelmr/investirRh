<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Galeria;
use App\Foto;
use Storage;
use Session;

class GaleriasController extends Controller
{
    public function index(){
			$galerias = Galeria::all();
			// dd($galerias);
			return view('admin.galerias.index', compact('galerias'));
		}

		public function create(){
			return view('admin.galerias.create');
		}

		public function store(Request $request){
			// dd($galeria);
		$dados = new Galeria();
		$galeria = $dados->fill($request->all());
		$galeria->save();
		// dd("oi");
		if ($request->hasFile('fotos')) {
				//pego as informações do arquivo e jogo na variavel
				$arquivo = $request->file('fotos');
				foreach ($arquivo as $arq ) {
					$extensao = $arq->extension(); // extensão do arquivo
				if ($extensao == 'jpg' || $extensao == 'jpeg' || $extensao == 'png' ) {
					$anexo = new Foto();
					$anexo->galeria_id = $galeria->id;
					$anexo->nome = $arq->getClientOriginalName(); //Nome do arquivo
					$arq->storeAs('public/', $anexo->nome);
					$anexo->save();
					// return response()->json("Cadastrado com sucesso!");
				}else{
					Session::flash('msg-error', 'Erro arquivo não compativel!');
					return view('admin/galerias');
					// return response()->json("Tipo de arquivo não compativel!");
				}
			}
			Session::flash('msg-success', 'cadastrado com sucesso!');
			return redirect('admin/galerias');
		}
	}

	public function delete($id){
	$arquivo = Galeria::find($id);
	$anexos = Foto::where('galeria_id', $arquivo->id)->get();
	//se vetor for maior que 1 entra aqui...
	if (count($anexos) >= 1) {
		foreach ($anexos as $anexo) {
			if( isset($anexo) && $anexo->nome != null){
					Storage::disk('fotos')->delete($anexo->nome);
					$anexo->delete();
				}
			}
		$arquivo->delete();
		Session::flash('msg-success', 'Deletado com sucesso');
		return redirect()->back();
	}else{
			Session::flash('msg-error', 'Erro ao deletar arquivo');
			$arquivo->delete();
			return redirect()->back();
		}
	}


	//detalhes da galeria de fotos
	public function show($id){
		$galeria = Galeria::find($id);
		return view('admin.galerias.show', compact('galeria'));
	}

	public function deleteFoto($id){
		// dd($id);
		$foto = Foto::find($id);
		$foto->delete();
		Storage::disk('fotos')->delete($foto->nome);
		return redirect()->back();
	}


}
