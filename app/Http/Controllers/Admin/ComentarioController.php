<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Comentario;
use App\Blog;

class ComentarioController extends Controller
{
    public function show($id){
			$comentarios = Comentario::find($id);
			$blog = Blog::where('id', $comentarios->blog_id)->first();
			// dd($blog);
			return view ('admin.comentario.show',compact('comentarios', 'blog'));
		}

		public function aprovar($id){
			$comentarios = Comentario::find($id);
			//dd($comentarios);
			$comentarios->ativo = 1;
			$comentarios->save();
			return redirect('/admin/home');
		}

		public function delete($id){
			$comentarios = Comentario::find($id);
			$comentarios->delete();
			//dd($comentarios);
			return redirect('/admin/home');
		}
}
