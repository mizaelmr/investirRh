<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\BlogRequest;
use App\Blog;
use Session;

class BlogController extends Controller
{
	public function index(){
		$dados = Blog::all();
		//dd($dados);
		return view('admin.blog.index',compact('dados'));
	}

	public function create(){
		$categorias = Blog::all();
		return view('admin.blog.create', compact('blog'));
	}

	public function store(BlogRequest $request){
		$store = new Blog();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		// dd($store);
		if($request->hasFile('foto')){
			$file = $request->file('foto');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->foto->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$store->foto = $fileName;
		}
		$store->save();
		Session::flash('msg', 'Cadastrado com sucesso');
		return redirect('/admin/blog');
	}



	public function edit($id){
		$blog = Blog::find($id);
		return view('admin.blog.edit', compact('blog'));
	}

	public function update(BlogRequest $request, $id){
		$blog = Blog::find($id);
		$usuario_id = Auth()->user()->id;
		$blog->fill($request->all());
		$blog->users_id = $usuario_id;
		if($request->hasFile('foto')){
			$file = $request->file('foto');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->foto->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$blog->foto = $fileName;
		}
		$blog->save();
		Session::flash('msg', 'Cadastrado com sucesso');
		return redirect('/admin/blog');
	}

	public function show($id){
		$blog = Blog::find($id);
		return view('admin.blog.show', compact('blog'));
	}

	public function delete($id){
		$arquivos = Blog::find($id);
		if($arquivos->foto != null){
			Storage::delete("public/$arquivos->foto");
		}
		$arquivos->delete();
		Session::flash('msg', 'Deletado com sucesso');
		return redirect('/admin/blog');
	}

}
