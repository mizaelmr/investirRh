<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CursoRequest;
use App\Curso;
use Session;

class CursoController extends Controller
{
	public function index(){
		$dados = Curso::all();
		//dd($dados);
		return view('admin.curso.index',compact('dados'));
	}

	public function create(){
		return view('admin.curso.create');
	}

	public function store(CursoRequest $request){
		$store = new Curso();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		//dd($request->hasFile('arquivo'));
		if($request->hasFile('arquivo')){
			$file = $request->file('arquivo');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->arquivo->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$store->arquivo = $fileName;
			$store->save();
			Session::flash('msg', 'Cadastrado com sucesso');
			return redirect('/admin/cursos');
		}else{
			Session::flash('msg', 'Erro ao cadastrar');
			return redirect('/admin/cursos');
		}
	}


	public function delete($id){
		$arquivos = Curso::find($id);
		//dd($arquivos->arquivo);
		if($arquivos->arquivo != null){
			Storage::delete("public/$arquivos->arquivo");
		}
		$arquivos->delete();
		Session::flash('msg', 'Deletado com sucesso');
		return redirect('/admin/cursos');
	}

		public function show($id){
			$ferramenta = Curso::find($id);
			return view('admin.curso.show', compact('ferramenta'));
		}

		public function edit($id){
			$dados = Curso::find($id);
			//dd($dados);
			return view('admin.curso.edit',compact('dados'));
		}

		public function update(CursoRequest $request, $id){
			$produto = Curso::find($id);
			//dd($produto);
			$usuario_id = Auth()->user()->id;
			$produto->fill($request->all());
			$produto->users_id = $usuario_id;
			if($request->hasFile('arquivo')){
				$file = $request->file('arquivo');
				$fileName = rand(1, 999) . $file->getClientOriginalName();
				$request->arquivo->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
				$produto->arquivo = $fileName;
			}
			$produto->save();
			Session::flash('msg', 'Cadastrado com sucesso');
			return redirect('/admin/cursos');
		}
}
