<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\EmpresaRequest;
use App\Empresa;
use Session;

class EmpresaController extends Controller
{
    public function index(){
      $dados = Empresa::all();
      // dd($dados);
      return view('admin.empresa.index', compact('dados'));
    }

    public function empresa($id){
      $info = empresa::find($id);
      return view('admin.empresa.edit', compact('info'));
    }
    public function update(EmpresaRequest $request, $id){
			$texto = Empresa::find($id);
		 	$dados = new Empresa();
			$usuario_id = Auth()->user()->id;
			$dados->fill($request->all());
			$texto->titulo = $dados->titulo;
			$texto->descricao = $dados->descricao;
			$texto->users_id = $usuario_id;
			//dd($texto);
			$texto->save();
			Session::flash('msg', 'Edição atualizada com Sucesso!');
      return redirect('/admin/empresa');
			// return "Cadastrando..";
		}
}
