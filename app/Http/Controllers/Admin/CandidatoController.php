<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\DadosCadastrais;
Use App\Ensino;
Use App\Experiencia;
use App\User;
use PDF;

class CandidatoController extends Controller
{
	public function index(){
		$candidato = User::where('tipo_users', 2)->get();
		//dd($candidato);
		return view('admin.candidato.index',compact('candidato'));
	}

	public function curriculo($id){
		$user = User::find($id);
		$dados = DadosCadastrais::where('users_id', $id)->first();
		$ensino = Ensino::where('users_id', $id)->get();
		$experiencia = Experiencia::where('users_id', $id)->get();
		//dd($experiencia);
		return view('admin.curriculo.index', compact('user','dados','ensino','experiencia'));
	}

	public function curriculoPdf($id) {

		$user = User::find($id);
		$dados = DadosCadastrais::where('users_id', $id)->first();
		$ensino = Ensino::where('users_id', $id)->get();
		$experiencia = Experiencia::where('users_id', $id)->get();
		//PDF AQUI

	}

}
