<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Contato;
use App\Mail\EmailContato;
use Session;

class ContatoController extends Controller
{
	public function index(){
		$dados = Contato::where("status",0)->get();
		//dd($dados);
		return view('admin.contato.index',compact('dados'));
	}

	public function contato($id){
		$info = contato::find($id);
		return view('admin.contato.resposta', compact('info'));
	}

	public function email(Request $email, $id){
		$dados = contato::find($id);
		$dados->resposta = $email->resposta;
		$dados->status = 1;
		$dados->save();
		// dd($dados->email);
		// Mail::to('mizaelmr@gmail.com')->send(new ContatoMail($email));
		Mail::to($dados->email)->send(new EmailContato($dados));
			Session::flash("msg", "Mensagem enviada com sucesso!");
			return redirect("/admin/contato");

	}
}
