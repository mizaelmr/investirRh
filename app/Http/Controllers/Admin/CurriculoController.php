<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\CurriculoRequest;
use App\DadosCadastrais;
Use App\Curriculo;
Use App\Ensino;
Use App\Experiencia;
use App\User;
use Session;


class CurriculoController extends Controller
{
	public function index(){
		$cara = Auth()->user()->id;
		$user = User::find($cara);
		$curriculo = Curriculo::where('users_id', $cara)->first();
		$dados = DadosCadastrais::where('users_id', $cara)->first();
		$ensino = Ensino::where('users_id', $cara)->get();
		$experiencia = Experiencia::where('users_id', $cara)->get();
		//dd($experiencia);
		return view('admin.curriculo.index', compact('user','curriculo','dados','ensino','experiencia'));
	}
	public function curriculo($id){
		$info = curriculo::find($id);
		//dd($info);
		return view('admin.curriculo.edit', compact('info'));
	}
	public function update(CurriculoRequest $request, $id){
		$curriculo = Curriculo::find($id);
		$curriculo->fill($request->all());
		//dd($curriculo);
		$curriculo->save();
		Session::flash('msg', 'Cadastrado com sucesso');
		return redirect('/curriculo');
	}


}
