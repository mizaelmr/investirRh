<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Session;
use Illuminate\Http\Request;
use App\User;
use App\Categorias;

class MenuController extends Controller
{
	public function conta(){
		$admin = User::where('tipo_users', 1)->get();
		//dd($admin);
		return view('admin.menu.conta.index', compact('admin'));
	}

	public function createConta(){
		return view('admin.menu.conta.create');
	}

	public function newUser(Request $request){
		$store = new User();
		$user = User::all();
		//dd($user);
		$store->fill($request->all());

		if ($store->password === $request->confirm_password) {
			$store->password = bcrypt($request->password);
			//dd($store->password);
		}
		else{
			Session::flash('msg', 'As senhas não coincidem!');
			return view('admin.menu.conta.create');
		}
		foreach ($user as $usuario) {
			if ($usuario->email === $store->email) {
				Session::flash('msg', 'Email já cadastrado!');
				return view('admin.menu.conta.create');
			}
		}
		$store->save();
		Session::flash('msg', 'Suas definições foram atualizadas com Sucesso!');
		return redirect('/admin/contas');
	}

	public function deleteUser($id){
	$user = User::find($id);
	//dd($user);
	$user->delete();
	Session::flash('msg', 'Usuário deletado com Sucesso!');
	return redirect('/admin/contas');
	}

	//CATEGORIA

	public function categoria(){
		$categoria = Categorias::all();
		//dd($categoria);
		return view('admin.menu.categoria.index', compact('categoria'));
	}

	public function createCateg(){
		return view('admin.menu.categoria.create');
	}

	public function storeCateg(Request $request){
		$store = new Categorias();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		//dd($store);
		$store->save();
		Session::flash('msg', 'Suas definições foram atualizadas com Sucesso!');
		return redirect('/admin/categorias');
	}

	public function deleteCateg($id){
	$categoria = Categorias::find($id);

	$categoria->delete();
	Session::flash('msg', 'Categoria deletada com Sucesso!');
	return redirect('/admin/categorias');
	}



}
