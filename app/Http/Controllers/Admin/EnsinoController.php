<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Http\Requests\EnsinoRequest;
use Illuminate\Http\Request;
use App\Ensino;
use Session;
use Gate;

class EnsinoController extends Controller
{
	public function create(){
		$cara = Auth()->user()->id;
		return view('admin.dados..ensino.create', compact('cara'));
	}

	public function store(EnsinoRequest $request){
		$store = new Ensino();
		$usuario_id = Auth()->user()->id;
		$store->fill($request->all());
		$store->users_id = $usuario_id;
		$store->save();
		Session::flash('msg', 'Cadastrado com sucesso');
		return redirect('/admin/dados');
	}

	public function edit($id){
		$dados = Ensino::find($id);

		if( Gate::denies('user_dados', $dados)){
			// abort(403, 'Não autorizado');
			return redirect('/admin/dados');
		}

		return view('admin.dados.ensino.edit', compact('dados'));
	}

	public function update(EnsinoRequest $request, $id){
		$texto = Ensino::find($id);
		$dados = new Ensino();

		if( Gate::denies('user_dados', $texto)){
			// abort(403, 'Não autorizado');
			return redirect('/admin/dados');
		}

		$dados->fill($request->all());
		$texto->instituicao = $dados->instituicao;
		$texto->curso = $dados->curso;
		$texto->descricao = $dados->descricao;
		$texto->dataInic = $dados->dataInic;
		$texto->dataFim = $dados->dataFim;
		$texto->save();
		Session::flash('msg', 'Edição atualizada com Sucesso!');
		return redirect('/admin/dados');
	}

	public function delete($id){
		$exp = Ensino::find($id);
		$exp->delete();
		Session::flash('msg', 'Instituição excluída com Sucesso!');
		return redirect('/admin/dados');
	}

}
