<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Http\Requests\DadosRequest;
use Auth;
use App\DadosCadastrais;
use App\Ensino;
use App\Experiencia;
use App\User;
use Gate;
use Session;

class DadosCadastraisController extends Controller
{
	public function index(){
		$cara = Auth::User()->id;
		$user = User::find($cara);
		$dados = DadosCadastrais::where('users_id', $cara)->first();
		$experiencia = Experiencia::where('users_id', $cara)->get();
		$ensino = Ensino::where('users_id', $cara)->get();
		//dd($dados);
		return view('admin.dados.index', compact('user', 'dados', 'experiencia','ensino'));
	}

	public function dados(){
		$cara = Auth::user()->id; // pega o logado
		$user = User::find($cara); //Busca no banco
		$dados = DadosCadastrais::where('users_id', $cara)->first();
		return view('admin.dados.edit', compact('user', 'dados'));
	}


	public function update(DadosRequest $request, $id){
		$dadosUser = DadosCadastrais::where('users_id', $id)->first();
		$userAuth = User::find($id);

		if($dadosUser == null){
			$dadosUser = new DadosCadastrais();
		}
		$dados = new DadosCadastrais();
		//dd($dados);
		$user = new User();

		$dados->fill($request->all());
		$user = $request->input('email');
		//dd($dados);

		$dadosUser->cep = $dados->cep;

		$dadosUser->nomeComp = $dados->nomeComp;
		$dadosUser->nascimento = $dados->nascimento;
		$dadosUser->cnh = $dados->cnh;
		$dadosUser->rua = $dados->rua;
		$dadosUser->numero = $dados->numero;
		$dadosUser->bairro = $dados->bairro;
		$dadosUser->cidade = $dados->cidade;
		$dadosUser->estado = $dados->estado;
		// $dadosUser->instagram = $dados->instagram;
		// $dadosUser->facebook = $dados->facebook;
		$dadosUser->atuacao = $dados->atuacao;
		$dadosUser->tel = $dados->tel;
		$dadosUser->cel = $dados->cel;
		$dadosUser->users_id = $id;
		$userAuth->name = $dados->nomeComp;

		// $this->authorize('user_dados', $dadosUser)
		if( Gate::denies('user_dados', $dadosUser)){
			// abort(403, 'Não autorizado');
			return redirect('/admin/dados');
		}

		if($request->hasFile('foto')){
			$file = $request->file('foto');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->foto->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$dadosUser->foto = $fileName;

		}
		//dd($dadosUser);
		$userAuth->email = $user;

		$dadosUser->save();
		$userAuth->save();
		Session::flash('msg', 'Edição atualizada com Sucesso!');
		return redirect('/admin/dados');
		// return "Cadastrando..";
	}


}
