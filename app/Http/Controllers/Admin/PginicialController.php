<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\PginicialRequest;
use App\Pginicial;
use Session;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;


class PginicialController extends Controller
{
	public function index(){
		$dados = Pginicial::all();
		//dd($dados);
		return view('admin.pginicial.index', compact('dados'));
	}
	public function pginicial($id){
		$info = pginicial::find($id);
		return view('admin.pginicial.edit', compact('info'));
	}
	public function update(PginicialRequest $request, $id){
		$texto = Pginicial::find($id);
		$dados = new Pginicial();
		$usuario_id = Auth()->user()->id;
		$dados->fill($request->all());
		$texto->titulo = $dados->titulo;
		$texto->descricao = $dados->descricao;
		$texto->users_id = $usuario_id;
		if($request->hasFile('foto')){
			$file = $request->file('foto');
			$fileName = rand(1, 999) . $file->getClientOriginalName();
			$request->foto->storeAs('public', $fileName ); // move o arquivo para ( storage / public)
			$texto->foto = $fileName;
		}
		//dd($texto);
		$texto->save();
		Session::flash('msg', 'Edição atualizada com Sucesso!');
		return redirect('/admin/pginicial');
		// return "Cadastrando..";
	}
}
