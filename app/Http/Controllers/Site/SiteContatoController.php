<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Contato;
use Auth;
use Session;


class SiteContatoController extends Controller
{
	public function index(){
		return view ('site.contato');
	}

	public function form(Request $request){
		$contato = new Contato();
		$contato->fill($request->all());
		$contato->status = 0;
		//dd($contato);
		$contato->save();
		Session::flash("msg", "Mensagem enviada com sucesso!");
		return view ('site.contato');
	}
}
