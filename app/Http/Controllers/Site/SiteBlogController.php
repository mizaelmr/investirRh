<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Produtos;
use App\Blog;
use App\Comentario;

class SiteBlogController extends Controller
{
	public function index(){
		$produtos = Produtos::all();
		$blogs = Blog::orderBy('id', 'desc')->paginate(15);
		return view('site.blog', compact('produtos', 'blogs'));
	}

	public function detalhes($id){
		$blogs = Blog::find($id);
		$comentarios = Comentario::where('blog_id', $id)->where('ativo', 1)->get();
		//dd($comentarios);
		return view('site.detalhesBlog', compact('blogs','comentarios'));
	}
}
