<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Produtos;

class SiteProdutoController extends Controller
{
    public function index(){
			$produtos = Produtos::all();
			//dd($produtos);
			return view ('site.produtos',compact('produtos'));
		}

		public function detalhes($id){
			$produto = Produtos::find($id);
			return view('site.detalhesProduto', compact('produto'));
		}
}
