<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Crypt;
use App\Vaga;

class SiteVagasController extends Controller
{
     public function index(){
			$CountVagas = Vaga::where('ativo', 1)->count();
			$vagas = Vaga::where('ativo', 1)->orderBy('id', 'desc')->paginate(10);
			return view('site.vagas', compact('vagas', 'CountVagas'));
		}

		public function buscar(){
			$CountVagas = Vaga::count();
			// $valor = Input::get('buscar');
			// dd($valor);
			$vagas = Vaga::where('titulo', Input::get('buscar'))
								->orWhere('titulo', 'like', '%' . Input::get('buscar') . '%')->paginate(15);
								//dd($vagas);
			return view('site.vagas', compact('vagas', 'CountVagas'));
		}

		public function detalhes($id){
			$vaga = Vaga::find($id);
			// dd($vaga);
			return view('site.detalhesVagas', compact('vaga'));

		}
}
