<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Comentario;

class SiteComentarioController extends Controller
{
    public function store(Request $request, $id){
			$comentario = new Comentario();
			$comentario->fill($request->all());
			$comentario->ativo = 0;
			$comentario->blog_id = $id;
			//dd($comentario);
			$comentario->save();
			return redirect()->back();
		}
}
