<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Pginicial;
use App\Clientes;
use App\Parceiros;
use App\Produtos;
use App\Vaga;
use App\Slide;

class SiteIndexController extends Controller
{
    public function index(){
			$dados = Pginicial::all();
			$clientes = Clientes::all();
			$parceiros = Parceiros::all();
			$produtos = Produtos::all();
			$slides = Slide::orderBy('nome', 'asc')->get();
			$vagas = Vaga::where('ativo', 1)->orderBy('id', 'desc')->paginate(5);
			return view('site.inicial', compact('dados', 'clientes', 'vagas', 'parceiros', 'produtos','slides'));
		}
}
