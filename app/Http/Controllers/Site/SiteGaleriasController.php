<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Galeria;
use App\Foto;

class SiteGaleriasController extends Controller
{
	public function index(){
		$galerias = Galeria::all();
		return view('site.galerias', compact('galerias'));
	}

	public function show($id){
		$galeria = Galeria::find($id);
		return view('site.detalhesGaleria', compact('galeria'));
	}
}
