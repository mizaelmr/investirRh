<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Curso;

class SiteCursoController extends Controller
{
	public function index(){
		$ferramentas = Curso::all();
		//dd($ferramentas);
		return view ('site.cursos',compact('ferramentas'));
	}

	public function detalhes($id){
		$curso = Curso::find($id);
		return view('site.detalhesCurso', compact('curso'));
	}
}
