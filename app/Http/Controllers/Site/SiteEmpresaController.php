<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Empresa;

class SiteEmpresaController extends Controller
{
    public function index(){
			$empresa = Empresa::all();
			return view ('site.empresa',compact('empresa'));
		}
}
