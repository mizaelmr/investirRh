<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DadosCadastrais;
use App\Experiencia;
use App\Contato;
use App\Blog;
use App\User;
use App\Vaga;
use App\Comentario;
use DB;
use Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
		{
				$countU = User::where('tipo_users', 2)->count();
				$countV = Vaga::count();
				$countB = Blog::count();
				$countM = Contato::where('status', 0)->count();

				$id = Auth::User()->id; //id do user logado
				$user = User::find($id); // localizou o user logado
				$ano = DadosCadastrais::where('users_id', $id)->select('nascimento', 'foto')->first();
				$exp = Experiencia::where('users_id', $id)->first();
				$dados = Blog::all(); // blog home clientes
				$blogHome = DB::table('blogs')->orderBy('created_at', 'asc')->paginate(5); // blog tabela home adm
				$comentarios = Comentario::where('ativo', 0)->paginate(5);
        return view('home', compact('dados','user', 'exp','ano', 'countU', 'countV', 'countM', 'countB', 'blogHome', 'comentarios'));
    }
}
