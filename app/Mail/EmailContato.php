<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailContato extends Mailable
{
    use Queueable, SerializesModels;

		public $dados;

	    public function __construct($dados)
	    {
	        $this->dados = $dados;
	    }


	    public function build()
	    {
	        return $this->from('emprego@investirh.com', 'Contato Investir Rh')
											->subject('Resposta InvestirH')
											->view('admin.contato.email');
	    }
}
