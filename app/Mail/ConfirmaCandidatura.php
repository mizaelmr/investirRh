<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConfirmaCandidatura extends Mailable
{
    use Queueable, SerializesModels;

		public $dados;

    public function __construct($dados)
    {
        $this->dados = $dados;
    }


    public function build()
    {
				return $this->from('contato@investirh.com', 'candidatura Investir Rh')
										->subject('Confirmação de candidatura InvestiRH')
										->view('admin.candidatura.email');
    }
}
