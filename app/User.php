<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password','tipo_users',
    ];
		public function dados(){
			return $this->belongsTo('App\DadosCadastrais', 'id');
		}

		public function candidatura(){
			return $this->hasMany('App\Candidatura', 'users_id');
		}

    protected $hidden = [
        'password', 'remember_token',
    ];
}
