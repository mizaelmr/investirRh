<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pginicial extends Model
{
	protected $fillable = [
			'id', 'descricao', 'titulo', 'users_id','foto',
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}
}
