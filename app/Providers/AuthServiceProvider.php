<?php

namespace App\Providers;

use App\Role;
use App\User;
use App\DadosCadastrais;
use App\Experiencia;
use App\Ensino;
// use Auth;
// use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(DadosCadastrais $dados)
    {
        $this->registerPolicies();

				Gate::define('admin_access', function (User $user) {
					return in_array($user->tipo_users, [1]);
				});

				Gate::define('client_access', function (User $user) {
					return in_array($user->tipo_users, [2]);
				});

				///proibir de editar outro ID

				Gate::define('user_dados', function (User $user, $dadosUser){
					return $user->id == $dadosUser->users_id;
				});


        //
    }
}
