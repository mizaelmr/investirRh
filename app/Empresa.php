<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
	protected $fillable = [
			'id', 'descricao', 'titulo', 'users_id'
	];

	public function usuario(){
		return $this->belongsTo('App\User', 'users_id');
	}
}
