<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
	protected $fillable = [
			'id','nome','email','comentario','ativo','blog_id',
	];
}
